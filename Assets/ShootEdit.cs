using UnityEngine;
using System.Collections.Generic;
using Assets.Resources.Data;

public class UIShootManager : MonoBehaviour
{
    [SerializeField] private MonkeySharedConfig _config;
    [SerializeField] private UIShootEdit _shootEditPrefab;
    [SerializeField] private Transform _container;

    private List<UIShootEdit> _shootEdits = new List<UIShootEdit>();

    private void Start()
    {
        if (_config == null)
        {
            Debug.LogError("MonkeySharedConfig non assegnato!");
            return;
        }

        CreateShootStandardEdits();
    }

    public void CreateShootStandardEdits()
    {
        ClearShootEdits();

        for (int i = 0; i < _config.ShootsStardard.Count; i++)
        {
            UIShootEdit shootEdit = Instantiate(_shootEditPrefab, _container);
            _config.ShootsStardard.ToArray()[i].Speed = 10;
            var shoot = _config.ShootsStardard[i];
            shootEdit.Set(i + 1, ref shoot);
            _shootEdits.Add(shootEdit);
        }
    }

    public void CreateShootIceEdits()
    {
        ClearShootEdits();

        for (int i = 0; i < _config.ShootsIce.Count; i++)
        {
            UIShootEdit shootEdit = Instantiate(_shootEditPrefab, _container);
            var shoot = _config.ShootsStardard[i];
            shootEdit.Set(i + 1, ref shoot);
            _shootEdits.Add(shootEdit);
        }
    }

    private void ClearShootEdits()
    {
        foreach (var shootEdit in _shootEdits)
        {
            if (shootEdit != null)
            {
                Destroy(shootEdit.gameObject);
            }
        }
        _shootEdits.Clear();
    }

    public void RefreshShootEdits()
    {
        CreateShootStandardEdits();
    }

    public IReadOnlyList<UIShootEdit> GetShootEdits()
    {
        return _shootEdits.AsReadOnly();
    }
}
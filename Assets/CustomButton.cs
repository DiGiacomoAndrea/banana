using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

[RequireComponent(typeof(Image))]
public class CustomSelectableImage : Selectable
{
    private Image _targetImage;
    private bool _isSelected;
    private bool _isPointerInside;

    public UnityEvent onClick = new UnityEvent();

    protected override void Awake()
    {
        base.Awake();
        _targetImage = GetComponent<Image>();

        if (colors.normalColor == default)
        {
            ColorBlock newColors = colors;
            newColors.normalColor = Color.white;
            newColors.highlightedColor = new Color(0.9f, 0.9f, 0.9f, 1f);
            newColors.selectedColor = new Color(0.8f, 0.8f, 0.8f, 1f);
            newColors.pressedColor = new Color(0.7f, 0.7f, 0.7f, 1f);
            newColors.disabledColor = new Color(0.5f, 0.5f, 0.5f, 0.5f);
            colors = newColors;
        }
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
        _isPointerInside = true;
        UpdateVisualState();
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);
        _isPointerInside = false;
        UpdateVisualState();
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        _isSelected = true; // Selezioniamo direttamente al click
        onClick.Invoke();
        UpdateVisualState();
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);
        UpdateVisualState();
    }

    protected override void DoStateTransition(SelectionState state, bool instant)
    {
        if (!gameObject.activeInHierarchy)
            return;

        Color targetColor = colors.normalColor;

        if (_isPointerInside && interactable)
        {
            if (state == SelectionState.Pressed)
            {
                targetColor = colors.pressedColor;
            }
            else
            {
                targetColor = colors.highlightedColor;
            }
        }
        else if (_isSelected && interactable)
        {
            targetColor = colors.selectedColor;
        }
        else if (!interactable)
        {
            targetColor = colors.disabledColor;
        }
        else
        {
            targetColor = colors.normalColor;
        }

        if (_targetImage != null)
        {
            _targetImage.CrossFadeColor(targetColor, instant ? 0f : colors.fadeDuration, true, true);
        }
    }

    public void Deselect()
    {
        _isSelected = false;
        UpdateVisualState();
    }

    private void UpdateVisualState()
    {
        DoStateTransition(currentSelectionState, false);
    }

    public void SetSelected(bool selected)
    {
        _isSelected = selected;
        UpdateVisualState();
    }

    public bool IsSelected()
    {
        return _isSelected;
    }
}
using Assets.Resources.Data;
using TMPro;
using UnityEngine;

public class UIShootEdit : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI _index;
    [SerializeField] private TMP_InputField _speed;
    [SerializeField] private TMP_InputField _angle;
    [SerializeField] private TMP_InputField _stamina;
    [SerializeField] private TMP_InputField _from;
    [SerializeField] private TMP_InputField _to;

    public void Set(int index, ref Shoot shoot)
    {
        shoot.Speed = 10;

        _index.text = index.ToString();
        _speed.text = shoot.Speed.ToString();
        _angle.text = shoot.Angle.ToString();
        _stamina.text = shoot.Stamina.ToString();
        _from.text = shoot.From.ToString();
        _to.text = shoot.To.ToString();
    }

}

using System;
using UnityEngine;

namespace Assets.Resources.Data
{
    #region MyRegion

    public enum PlayerEnum
    {
        player1,
        player2
    }

    #endregion

    [CreateAssetMenu(fileName = "MonkeyConfig", menuName = "Monkey Config")]
    public class MonkeyConfig : ScriptableObject
    {
        public PlayerEnum player;
        public int direction;
    }
}

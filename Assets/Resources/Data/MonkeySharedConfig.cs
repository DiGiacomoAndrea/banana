using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Resources.Data
{
    #region Types

    [Serializable]
    public struct Shoot : IComparable<Shoot>
    {
        public float Speed;
        public float Angle;
        public int Stamina;
        public int From;
        public int To;

        public override bool Equals(object obj)
        {
            if (!(obj is Shoot))
                return false;

            var other = (Shoot)obj;

            return Speed == other.Speed &&
                   Angle == other.Angle &&
                   Stamina == other.Stamina &&
                   From == other.From &&
                   To == other.To;
        }

        public int CompareTo(Shoot other)
        {
            return From.CompareTo(other.From);
        }

        public static bool operator ==(Shoot shoot1, Shoot shoot2)
        {
            return shoot1.Equals(shoot2);
        }

        public static bool operator !=(Shoot shoot1, Shoot shoot2)
        {
            return !shoot1.Equals(shoot2);
        }
    }

    public static class ShootsExtension
    {
        public static Shoot? Get(this IList<Shoot> shoots, double millisecondElapsed)
        {
            if(millisecondElapsed == 0) return null;

            var max = shoots.Max(x => x);
            if (millisecondElapsed > max.From) return max;

            var elapsedMilliseconds = Convert.ToInt32(millisecondElapsed);
            return shoots
                .Cast<Shoot?>()
                .FirstOrDefault(x =>
                    (x?.From <= elapsedMilliseconds && x?.To >= elapsedMilliseconds));
        }

        public static int GetTotalRange(this IList<Shoot> shoots)
        {
            return shoots.Max(x => x.To) - shoots.Min(x => x.From);
        }

        public static float GetMaxStamina(this IList<Shoot> shoots, Shoot shoot)
        {
            var orderShoots = shoots.OrderBy(x => x.Stamina);
            var shootIndex = orderShoots.ToList().IndexOf(shoot);

            if (shootIndex >= 1)
                return shoot.Stamina - orderShoots.ElementAt(shootIndex - 1).Stamina;
            
            return shoot.Stamina;
        }
    }

    #endregion

    [CreateAssetMenu(fileName = "MonkeySharedConfig", menuName = "Monkey Shared Config")]
    public class MonkeySharedConfig : ScriptableObject
    {
        public int win;
        public int life;
        public float moveSpeed;
        public float MaxStamina;
        public List<Shoot> ShootsStardard;
        public List<Shoot> ShootsIce;
    }
}
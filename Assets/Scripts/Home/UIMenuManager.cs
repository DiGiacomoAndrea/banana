using UnityEngine;
using UnityEngine.UI;

public class UIMenuManager : MonoBehaviour
{
    [SerializeField] private GameObject _homePanel;
    [SerializeField] private GameObject _multiplayerPanel;
    [SerializeField] private Button _firstButtonHome;
    [SerializeField] private Button _firstMultiplayerPanel;
    [SerializeField] private GameObject _exitBtn;

    private void Awake()
    {

#if UNITY_WEBGL
        _homePanel.GetComponent<VerticalLayoutGroup>().spacing = -70;
        _exitBtn.SetActive(false);
#endif
        ShowHomePanel();
    }

    public void ShowHomePanel()
    {
        _homePanel.gameObject.SetActive(true);
        _multiplayerPanel.gameObject.SetActive(false);
        _firstButtonHome.Select();
    }

    public void ShowHomeMultiplayerPanel()
    {
        _homePanel.gameObject.SetActive(false); 
        _multiplayerPanel.gameObject.SetActive(true);
        _firstMultiplayerPanel.Select();
    }
}

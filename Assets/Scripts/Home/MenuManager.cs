using FishNet;
using FishNet.Managing;
using FishNet.Managing.Scened;
using FishNet.Transporting.UTP;
using System;
using System.Threading.Tasks;
using FishNet.Transporting;
using TMPro;
using Unity.Services.Authentication;
using Unity.Services.Core;
using Unity.Services.Relay;
using UnityEngine;
using Unity.Networking.Transport.Relay;
using Unity.Services.Relay.Models;

namespace Assets.Scripts.Local
{
    public class MenuManager : MonoBehaviour
    {
        #region struct

        private struct RelayJoinData
        {
            public string IPv4Address;
            public ushort Port;
            public Guid AllocationID;
            public byte[] AllocationIDBytes;
            public byte[] ConnectionData;
            public byte[] HostConnectionData;
            public byte[] Key;
        }

        private struct RelayHostData
        {
            public string JoinCode;
            public string IPv4Address;
            public ushort Port;
            public Guid AllocationID;
            public byte[] AllocationIDBytes;
            public byte[] ConnectionData;
            public byte[] Key;
        }

        #endregion

        [SerializeField] private TMP_InputField _joinCode;
        [SerializeField] private GameObject _networkManagerGameObject;

        private NetworkManager _networkManager;
        private SceneManager _sceneManager;
        private FishyUnityTransport _unityTransport;

        void Start()
        {
            if (Time.timeScale < 1)
                Time.timeScale = 1;

            Cursor.visible = true;
        }

        public void StartLocal()
        {
            SetAutomaticPhysicsSimulation(true);
            UnityEngine.SceneManagement.SceneManager.LoadScene("BananaLocalScene");
        }

        public async void StartHost()
        {
            ActiveMultiplayer();

            InstanceFinder.NetworkManager.ServerManager.OnServerConnectionState += OnServerConnectionState;

            var joinCode = await StartHostWithRelay(2);

            PlayerPrefs.SetString("joinCode", joinCode);
            Debug.Log("Join Code :) -> " + joinCode);
        }

        private void ActiveMultiplayer()
        {
            _networkManagerGameObject.SetActive(true);
            _networkManager = _networkManagerGameObject.GetComponent<NetworkManager>();
            _unityTransport = _networkManager.TransportManager.GetTransport<FishyUnityTransport>();
            _sceneManager = _networkManager.SceneManager;
        }

        public async void StartClient()
        {
            ActiveMultiplayer();

            var clientData = await StartClientWithRelay(_joinCode.text.Trim());

            PlayerPrefs.SetString("joinCode", _joinCode.text.Trim());
        }

        public void Exit() => Application.Quit();

        private void OnServerConnectionState(ServerConnectionStateArgs obj)
        {
            if (obj.ConnectionState == LocalConnectionState.Started)
            {
                InstanceFinder.NetworkManager.ServerManager.OnServerConnectionState -= OnServerConnectionState;

                var sld = new SceneLoadData("BananaMultiplayerScene") { ReplaceScenes = ReplaceOption.All };
                _sceneManager.LoadGlobalScenes(sld);
            }
        }

        private void SetAutomaticPhysicsSimulation(bool automatic)
        {

        }

        public async Task<string> StartHostWithRelay(int maxConnections = 5)
        {
            //Initialize the Unity Services engine
            await UnityServices.InitializeAsync();
            //Always authenticate your users beforehand
            if (!AuthenticationService.Instance.IsSignedIn)
            {
                //If not already logged, log the user in
                await AuthenticationService.Instance.SignInAnonymouslyAsync();
            }

            // Request allocation and join code
            Allocation allocation = await RelayService.Instance.CreateAllocationAsync(maxConnections);
            var joinCode = await RelayService.Instance.GetJoinCodeAsync(allocation.AllocationId);

            // Configure transport
#if UNITY_WEBGL
            var unityTransport = _networkManager.TransportManager.GetTransport<FishyUnityTransport>();
            unityTransport.SetRelayServerData(new RelayServerData(allocation, "wss"));
            unityTransport.UseWebSockets = true;
#else
            var unityTransport = _networkManager.TransportManager.GetTransport<FishyUnityTransport>();
            unityTransport.SetRelayServerData(new RelayServerData(allocation, "dtls"));
#endif

            // Start host
            if (_networkManager.ServerManager.StartConnection()) // Server is successfully started.
            {
                _networkManager.ClientManager.StartConnection();
                return joinCode;
            }
            return null;
        }

        public async Task<bool> StartClientWithRelay(string joinCode)
        {
            //Initialize the Unity Services engine
            await UnityServices.InitializeAsync();
            //Always authenticate your users beforehand
            if (!AuthenticationService.Instance.IsSignedIn)
            {
                //If not already logged, log the user in
                await AuthenticationService.Instance.SignInAnonymouslyAsync();
            }
             
            // Join allocation
            var joinAllocation = await RelayService.Instance.JoinAllocationAsync(joinCode: joinCode);

            // Configure transport
#if UNITY_WEBGL
            var unityTransport = _networkManager.TransportManager.GetTransport<FishyUnityTransport>();
            unityTransport.SetRelayServerData(new RelayServerData(joinAllocation, "wss"));
            unityTransport.UseWebSockets = true;
#else
            var unityTransport = _networkManager.TransportManager.GetTransport<FishyUnityTransport>();
            unityTransport.SetRelayServerData(new RelayServerData(joinAllocation, "dtls"));
#endif
            // Start client
            return !string.IsNullOrEmpty(joinCode) && _networkManager.ClientManager.StartConnection();
        }
    }
}
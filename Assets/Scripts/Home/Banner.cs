using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;

public class Banner : MonoBehaviour
{
    [SerializeField] private float scrollSpeed = 100f;
    [SerializeField] private float spaceBetweenMessages = 100f;
    [TextArea(3, 5)]
    [SerializeField] private List<string> messages = new List<string>();
    [SerializeField] private GameObject textPrefab;

    private ScrollRect scrollRect;
    private RectTransform contentContainer;
    private RectTransform viewportRect;
    private List<TextMeshProUGUI> textObjects = new List<TextMeshProUGUI>();
    private float totalWidth;
    private float viewportWidth;
    private Vector2 startPosition;
    private bool isInitialized;
    private float lastScreenWidth;
    private Canvas parentCanvas;

    void Awake()
    {
        SetupScrollView();
    }

    void SetupScrollView()
    {
        var scrollViewObj = new GameObject("ScrollView", typeof(RectTransform), typeof(ScrollRect));
        scrollViewObj.transform.SetParent(transform, false);
        scrollRect = scrollViewObj.GetComponent<ScrollRect>();

        var scrollRectTransform = scrollViewObj.GetComponent<RectTransform>();
        scrollRectTransform.anchorMin = Vector2.zero;
        scrollRectTransform.anchorMax = Vector2.one;
        scrollRectTransform.sizeDelta = Vector2.zero;

        var viewportObj = new GameObject("Viewport", typeof(RectTransform), typeof(RectMask2D));
        viewportObj.transform.SetParent(scrollViewObj.transform, false);
        viewportRect = viewportObj.GetComponent<RectTransform>();

        viewportRect.anchorMin = Vector2.zero;
        viewportRect.anchorMax = Vector2.one;
        viewportRect.sizeDelta = Vector2.zero;

        var contentObj = new GameObject("Content", typeof(RectTransform), typeof(HorizontalLayoutGroup), typeof(ContentSizeFitter));
        contentObj.transform.SetParent(viewportObj.transform, false);
        contentContainer = contentObj.GetComponent<RectTransform>();

        contentContainer.anchorMin = new Vector2(0, 0);
        contentContainer.anchorMax = new Vector2(0, 1);
        contentContainer.pivot = new Vector2(0, 0.5f);

        var horizontalLayout = contentObj.GetComponent<HorizontalLayoutGroup>();
        horizontalLayout.spacing = spaceBetweenMessages;
        horizontalLayout.childAlignment = TextAnchor.MiddleLeft;
        horizontalLayout.childForceExpandWidth = false;
        horizontalLayout.childForceExpandHeight = false;
        horizontalLayout.padding = new RectOffset(0, 0, 0, 0);

        var contentSizeFitter = contentObj.GetComponent<ContentSizeFitter>();
        contentSizeFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;

        scrollRect.content = contentContainer;
        scrollRect.viewport = viewportRect;
        scrollRect.horizontal = true;
        scrollRect.vertical = false;
        scrollRect.movementType = ScrollRect.MovementType.Unrestricted;
        scrollRect.inertia = false;
        scrollRect.elasticity = 0;
        scrollRect.decelerationRate = 0;

        contentContainer.gameObject.SetActive(false);
    }

    void Start()
    {
        parentCanvas = GetComponentInParent<Canvas>();
        lastScreenWidth = GetScreenWidth();
        InitializeTextObjects();
        StartCoroutine(InitializeAfterLayout());
    }

    float GetScreenWidth()
    {
        if (parentCanvas.renderMode == RenderMode.ScreenSpaceOverlay)
        {
            return Screen.width;
        }
        return parentCanvas.GetComponent<RectTransform>().rect.width;
    }

    void CheckForResize()
    {
        var currentScreenWidth = GetScreenWidth();
        if (!Mathf.Approximately(lastScreenWidth, currentScreenWidth))
        {
            lastScreenWidth = currentScreenWidth;
            RecalculateLayout();
        }
    }

    void RecalculateLayout()
    {
        StartCoroutine(RecalculateLayoutCoroutine());
    }

    System.Collections.IEnumerator RecalculateLayoutCoroutine()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        Canvas.ForceUpdateCanvases();
        LayoutRebuilder.ForceRebuildLayoutImmediate(contentContainer);

        CalculateTotalWidth();

        var currentPos = contentContainer.anchoredPosition;
        if (currentPos.x > startPosition.x)
        {
            contentContainer.anchoredPosition = startPosition;
        }
    }

    System.Collections.IEnumerator InitializeAfterLayout()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        CalculateTotalWidth();
        contentContainer.anchoredPosition = startPosition;
        contentContainer.gameObject.SetActive(true);
        isInitialized = true;
    }

    void InitializeTextObjects()
    {
        foreach (var txt in textObjects)
        {
            if (txt != null)
            {
                Destroy(txt.gameObject);
            }
        }
        textObjects.Clear();

        foreach (var message in messages)
        {
            var newTextObj = Instantiate(textPrefab, contentContainer);
            var newText = newTextObj.GetComponent<TextMeshProUGUI>();

            if (newText == null)
            {
                Debug.LogError("The provided prefab doesn't have a TextMeshProUGUI component.");
                return;
            }

            var textRect = newText.GetComponent<RectTransform>();
            textRect.anchorMin = new Vector2(0, 0);
            textRect.anchorMax = new Vector2(0, 1);

            newText.text = message;
            textObjects.Add(newText);
        }
    }

    void CalculateTotalWidth()
    {
        Canvas.ForceUpdateCanvases();
        LayoutRebuilder.ForceRebuildLayoutImmediate(contentContainer);

        totalWidth = 0f;
        foreach (TextMeshProUGUI text in textObjects)
        {
            if (text != null)
            {
                var rectTransform = text.GetComponent<RectTransform>();
                totalWidth += rectTransform.rect.width;
            }
        }

        // Aggiungi lo spazio tra i messaggi
        totalWidth += spaceBetweenMessages * (textObjects.Count - 1);

        viewportWidth = viewportRect.rect.width;
        startPosition = new Vector2(viewportWidth, 0);
    }

    void ResetPosition()
    {
        contentContainer.anchoredPosition = startPosition;
    }

    void Update()
    {
        if (!isInitialized || totalWidth == 0)
            return;

        CheckForResize();

        var position = contentContainer.anchoredPosition;
        position.x -= scrollSpeed * Time.deltaTime;

        // Calcola la posizione effettiva dell'ultimo elemento
        float rightEdgePosition = position.x + totalWidth;

        if (rightEdgePosition <= 0)
        {
            ResetPosition();
        }
        else
        {
            contentContainer.anchoredPosition = position;
        }
    }

    void OnDestroy()
    {
        foreach (var txt in textObjects)
        {
            if (txt != null)
            {
                Destroy(txt.gameObject);
            }
        }
    }
}
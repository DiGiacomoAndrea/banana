using UnityEngine;

namespace Assets.Scripts.Shared
{
    public class SoundManager : MonoBehaviour
    {
        private AudioSource _sound;

        void Awake()
        {
            _sound = GetComponent<AudioSource>();
        }

        public void ToggleSound(bool value)
        {
            if (value)
                _sound.Play();
            else
                _sound.Pause();
        }
    }
}

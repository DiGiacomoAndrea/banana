using Assets.Resources.Data;
using Zenject;

namespace Assets.Scripts.Shared.UI
{
    public class UIStaminaIce : UIStamina
    {
        [Inject]
        public void Constructor(MonkeySharedConfig monkeySharedConfig)
        {
            Shoots = monkeySharedConfig.ShootsIce;
        }
    }
}
using Assets.Resources.Data;
using TMPro;
using UnityEngine;
using Zenject;

public class UILifeNEW : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _textLife;
    [SerializeField] private Animator animator;
    private MonkeySharedConfig _monkeySharedConfig;
    private int lifeCount;

    [Inject]
    public void Constructor(MonkeySharedConfig monkeySharedConfig)
    {
        _monkeySharedConfig = monkeySharedConfig;
        lifeCount = monkeySharedConfig.life;
    }

    public void Reset()
    {
        lifeCount = _monkeySharedConfig.life;
        _textLife.text = lifeCount.ToString();
    }

    public void Decrease()
    {
        lifeCount--;
        animator.Rebind();
    }

    public void RefreshText()
    {
        _textLife.text = lifeCount.ToString();
    }
}

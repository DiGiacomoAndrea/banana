using Assets.Resources.Data;
using Zenject;

namespace Assets.Scripts.Shared.UI
{
    public class UIStaminaStandard : UIStamina
    {
        [Inject]
        public void Constructor(MonkeySharedConfig monkeySharedConfig)
        {
            Shoots = monkeySharedConfig.ShootsStardard;
        }
    }
}
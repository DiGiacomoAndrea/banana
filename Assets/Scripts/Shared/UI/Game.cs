using Assets.Scripts.Shared.Utils;
using FishNet;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Shared.UI
{

    #region Types

    public interface IGame : IScene
    {
        void ActiveEasterEgg();

        void ResetScene();

        void SetGameEnd();

        void SetPause(bool value);
    }

    #endregion

    public class Game : Scene, IGame
    {
        [SerializeField] private List<Animator> _animators;
        [SerializeField] private float _slowGameOverDuration;
        [SerializeField] private AnimationCurve _slowGameOverAnimationCurve;
        [SerializeField] private Timer _timer;

        public override void Show()
        {
            gameObject.transform.localScale = new Vector3(1, 1, 1);
            IsVisible = true;
        }

        public override void Hide()
        {
            gameObject.transform.localScale = Vector3.zero;
            IsVisible = false;
        }

        public void ActiveEasterEgg()
        {
            BroadcastMessage("EasterEgg", SendMessageOptions.DontRequireReceiver);
        }

        public void ResetSceneGameOver()
        {
            BroadcastMessage("ResetGameOver");
            ResetScene();
        }

        public void ResetScene()
        {
            _animators.ForEach(x => x.Rebind());
            BroadcastMessage("Reset");
            //BroadcastMessage("SetExitDisable", false);
        }

        public void SetGameEnd()
        {
            //BroadcastMessage("SetExitDisable", true);
            //TODO LOCAL
            StartCoroutine("SlowTimeScale");
        }

        IEnumerator SlowTimeScale()
        {
            var elapasedTime = 0f;

            while (elapasedTime < _slowGameOverDuration)
            {
                var completePercentage = elapasedTime / _slowGameOverDuration;
                var slowFactory = 1f - _slowGameOverAnimationCurve.Evaluate(completePercentage);

                elapasedTime += Time.unscaledDeltaTime;
                Time.timeScale = slowFactory;

                yield return null;
            }

        }

        public void SetPause(bool value)
        {
            if (_timer.isActiveAndEnabled) value = true;

            BroadcastMessage("Pause", value);
            
            //:(
            if (InstanceFinder.IsOffline)
                Time.timeScale = value ? 0 : 1;
        }
    }
}

using UnityEngine;

namespace Assets.Scripts.Shared.UI
{
    public class UIShoots : MonoBehaviour
    {
        [SerializeField] private GameObject _shootStandard;
        [SerializeField] private GameObject _shootIce;
        [SerializeField] private Animator _animatorShootStandard;
        [SerializeField] private Animator _animatorShootIce;

        public void Change(ShootTypeEnum shootTypeEnum)
        {
            if (shootTypeEnum == ShootTypeEnum.Ice)
            {
                _shootStandard.SetActive(false);
                _shootIce.SetActive(true);
            }
            else
            {
                _shootStandard.SetActive(true);
                _shootIce.SetActive(false);
            }
        }

    }
}
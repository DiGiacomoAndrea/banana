using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Shared.UI
{
    public class UIReady : MonoBehaviour
    {
        private Slider _slider;

        void Awake()
        {
            _slider = GetComponent<Slider>();
        }

        public void UpdateSlider(float value)
        {
            if (_slider != null)
            {
                _slider.value = value;
            }
        }

        public void Reset()
        {
            _slider.value = 0;
        }
    }
}

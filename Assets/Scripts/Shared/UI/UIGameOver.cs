﻿using System.Collections.Generic;
using System.Linq;
using Assets.Resources.Data;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace Assets.Scripts.Shared.UI
{
    public class UIGameOver : Scene
    {
        [SerializeField] private Animator _gameEndAnimator;
        private IEnumerable<PlayerInput> _playerInputs;

        [Inject]
        public void Constructor(IEnumerable<PlayerInput> playerInputs)
        {
            _playerInputs = playerInputs;
        }

        public void Show(PlayerEnum looserPlayer) 
        {
            base.Show();


            if (looserPlayer == PlayerEnum.player1)
            {
                _gameEndAnimator.SetBool("WinPlayer2", true);
            }

            if (looserPlayer == PlayerEnum.player2)
            {
                _gameEndAnimator.SetBool("WinPlayer1", true);
            }

            DisablePlayersCurrentActionMap();
        }

        public override void Hide()
        {
            EnablePlayersCurrentActionMap();

            base.Hide();
        }


        private void EnablePlayersCurrentActionMap()
        {
            bool areBothPlayersActive = _playerInputs.ElementAt(0).isActiveAndEnabled &&
                                        _playerInputs.ElementAt(1).isActiveAndEnabled;

            if (areBothPlayersActive)
            {
                _playerInputs.ElementAt(0).currentActionMap.Enable();
                _playerInputs.ElementAt(1).currentActionMap.Enable();
            }
            else
            {
                _playerInputs.First(x => x.isActiveAndEnabled).currentActionMap.Enable();
            }
        }


        private void DisablePlayersCurrentActionMap()
        {
            bool areBothPlayersActive = _playerInputs.ElementAt(0).isActiveAndEnabled &&
                                        _playerInputs.ElementAt(1).isActiveAndEnabled;

            if (areBothPlayersActive)
            {
                _playerInputs.ElementAt(0).currentActionMap.Disable();
                _playerInputs.ElementAt(1).currentActionMap.Disable();
            }
            else
            {
                _playerInputs.First(x => x.isActiveAndEnabled).currentActionMap.Disable();
            }
        }

    }
}
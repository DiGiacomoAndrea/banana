using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Shared.UI
{
    public class UIShoot : MonoBehaviour
    {
        [SerializeField] private Slider slider;

        public void UpdateSlider(float pressTime) => slider.value = pressTime;  

    }
}

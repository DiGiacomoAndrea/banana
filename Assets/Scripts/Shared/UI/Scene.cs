using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.Shared.UI
{
    [SerializeField]
    public interface IScene
    {
        bool IsVisible { get; set; }
        void Show();
        void Hide();
    }

    public abstract class  Scene : MonoBehaviour, IScene
    {
        public bool IsVisible { get; set; }
        
        [SerializeField] [CanBeNull] private Button _firstButton;

        public void Awake()
        {
            IsVisible = gameObject.activeSelf;
        }

        public virtual void Show()
        {
            IsVisible = true;
            gameObject.SetActive(true);
            _firstButton?.Select();
        }

        public virtual void Hide()
        {
            IsVisible = false;
            gameObject.SetActive(false);
            EventSystem.current?.SetSelectedGameObject(null);
        }
    }

    public static class SceneExtension
    {
        public static void SetHides(this IList<IScene> scenes, Action<IList<IScene>> postHideAction)
        {
            foreach (var scene in scenes)
                if (scene.IsVisible)
                    scene.Hide();

            postHideAction(scenes);
        }

        [CanBeNull]
        public static T GetType<T>(this IEnumerable<IScene> gameScenes) where T : class, IScene
        {
            foreach (var gameScene in gameScenes)
                if (gameScene is T res)
                    return res;

            return default;
        }
    }
}

﻿using Assets.Resources.Data;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace Assets.Scripts.Shared.UI
{
    public class UIGameEnd : Scene
    {
        private GameEnd _gameEnd;

        [Inject]
        public void Constructor(GameEnd gameEnd)
        {
            _gameEnd = gameEnd;
        }

        public void Show(PlayerEnum looserPlayer)
        {
            base.Show();
            _gameEnd.Show(looserPlayer);
        }

        private void SetImageRotationZ(RectTransform rectTransform, float z)
        {
            var rotation = rectTransform.rotation;
            rectTransform.rotation = Quaternion.Euler(
                rotation.eulerAngles.x,
                rotation.eulerAngles.y,
                z);
        }

        public override void Hide()
        {
            _gameEnd.Hide();
            base.Hide();
        }

        public void ResetSceneGameOver() => _gameEnd.ResetSceneGameOver();
    }

    public class GameEnd
    {
        private Animator _animatorGameEndPlayer1;
        private Animator _animatorGameEndPlayer2;
        private IEnumerable<PlayerInput> _playerInputs;
        private IEnumerable<IReady> _readies;
        private UIWin _uiWinPlayer1;
        private UIWin _uiWinPlayer2;

        [Inject]
        public void Constructor(
            [Inject(Id = "gameEndPlayer1")] Animator animatorGameEndPlayer1,
            [Inject(Id = "gameEndPlayer2")] Animator animatorGameEndPlayer2,
            [Inject(Id = "UIWinPlayer1")] UIWin uiWinPlayer1,
            [Inject(Id = "UIWinPlayer2")] UIWin uiWinPlayer2,
            IEnumerable<PlayerInput> playerInputs,
            IEnumerable<IReady> readies)
        {
            _uiWinPlayer1 = uiWinPlayer1;
            _uiWinPlayer2 = uiWinPlayer2;
            _animatorGameEndPlayer1 = animatorGameEndPlayer1;
            _animatorGameEndPlayer2 = animatorGameEndPlayer2;
            _playerInputs = playerInputs;
            _readies = readies;
        }

        public void Show(PlayerEnum looserPlayer)
        {
            //switch control schema!
            SwitchPlayerInputActionMap("Player1 GameOver", "Player2 GameOver");
          
            if (looserPlayer == PlayerEnum.player2)
            {
                _animatorGameEndPlayer1.SetBool("Win", true);
                _animatorGameEndPlayer2.SetBool("Win", false);
                _uiWinPlayer1.Increment();
            }

            if (looserPlayer == PlayerEnum.player1)
            {
                _animatorGameEndPlayer1.SetBool("Win", false);
                _animatorGameEndPlayer2.SetBool("Win", true);
                _uiWinPlayer2.Increment();
            }
        }

        public void Hide()
        {
            //switch control schema!
            SwitchPlayerInputActionMap("Player1", "Player2");

            _readies.ToList().ForEach(x => x.ReadyReset());

            _uiWinPlayer1.Hide();
            _uiWinPlayer2.Hide();

            _animatorGameEndPlayer1.Rebind();
            _animatorGameEndPlayer2.Rebind();
        }

        private void SwitchPlayerInputActionMap(string activeActionMap, string fallbackActionMap)
        {
            bool areBothPlayersActive = _playerInputs.ElementAt(0).isActiveAndEnabled &&
                                        _playerInputs.ElementAt(1).isActiveAndEnabled;

            if (areBothPlayersActive)
            {
                _playerInputs.ElementAt(0).SwitchCurrentActionMap(activeActionMap);
                _playerInputs.ElementAt(1).SwitchCurrentActionMap(fallbackActionMap);
            }
            else
            {
                _playerInputs.First(x => x.isActiveAndEnabled).SwitchCurrentActionMap(activeActionMap);
            }
        }


        public void ResetSceneGameOver()
        {
            _uiWinPlayer1.Reset();
            _uiWinPlayer2.Reset();
        }
    }
}
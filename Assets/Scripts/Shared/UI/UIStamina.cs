using Assets.Resources.Data;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Shared.UI
{
    public class UIStamina : MonoBehaviour
    {

        [SerializeField] private GameObject _staminaSlider;
        [SerializeField] private float Slider_Height_Effect = 70;
        protected List<Shoot> Shoots { get; set; }
        private List<UISlider> _sliders = new();
        private float _sliderInitialHeight;

        void Start()
        {
            RemoveChilds();

            var widthContainer = transform.GetComponent<RectTransform>().rect.width;
            var totalRange = Shoots.GetTotalRange();

            foreach (var shoot in Shoots)
            {
                var maxStamina = Shoots.GetMaxStamina(shoot);
                var slider = InstantiateSlider(_staminaSlider, widthContainer, totalRange, shoot, maxStamina);
                _sliders.Add(slider);
            }

            _sliderInitialHeight = _staminaSlider.GetComponent<RectTransform>().rect.height;
        }

        UISlider InstantiateSlider(GameObject gameObject, float widthContainer, int totalRange, Shoot shoot,
            float maxStamina)
        {
            var widthSlider = GetWidthSlider(widthContainer, totalRange, shoot);
            var sliderGameObject = Instantiate(gameObject, transform);
            var slider = sliderGameObject.GetComponent<Slider>();

            sliderGameObject.GetComponent<RectTransform>().SetWidth(widthSlider);
            slider.maxValue = maxStamina;
            slider.value = slider.maxValue;

            return new UISlider
            {
                Slider = slider,
                Shoot = shoot
            };
        }


        public void SetStaminaShoot(float currentShootValue, bool hasShoot)
        {
            if (hasShoot)
            {
                var shoot = Shoots.Get(currentShootValue);
                _sliders.FirstOrDefault(x => x.Shoot == shoot)?.Slider.GetComponent<RectTransform>().SetHeight(Slider_Height_Effect);
                _sliders.Where(x => x.Shoot != shoot).ToList().ForEach(x =>
                {
                    x.Slider.GetComponent<RectTransform>().SetHeight(_sliderInitialHeight);
                });
            }
            else
            {
                _sliders.ForEach(x =>
                {
                    x.Slider.GetComponent<RectTransform>().SetHeight(_sliderInitialHeight);
                });
            }
        }

        public void Add(float currentStamina)
        {
            foreach (var slider in _sliders)
            {
                var sliderSlider = slider.Slider;
                if (currentStamina >= sliderSlider.value)
                {
                    sliderSlider.value = currentStamina;
                    currentStamina -= sliderSlider.value;
                }
            }
        }

        public void Decrease(float staminaShoot)
        {
            var reversSliders = new List<UISlider>();
            reversSliders.AddRange(_sliders);
            reversSliders.Reverse();

            foreach (var slider in reversSliders)
            {
                var prevValueSlider = slider.Slider.value;
                slider.Slider.value -= staminaShoot;
                staminaShoot -= prevValueSlider;
            }
        }

        public void Reset()
        {
            foreach (var slider in _sliders)
            {
                slider.Slider.GetComponent<RectTransform>().SetHeight(_sliderInitialHeight);
                slider.Slider.value = slider.Slider.maxValue;
            }
        }

        private float GetWidthSlider(float widthContainer, int totalRange, Shoot shoot)
        {
            var range = shoot.To - shoot.From;
            var widthSlider = widthContainer * range / totalRange;

            return widthSlider;
        }

        void RemoveChilds()
        {
            for (var i = transform.childCount - 1; i >= 0; i--)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }

    }

    internal class UISlider
    {
        public Slider Slider;
        public Shoot Shoot;
    }

    public static class RectTransformExtensions
    {
        public static void SetDefaultScale(this RectTransform trans)
        {
            trans.localScale = new Vector3(1, 1, 1);
        }

        public static void SetPivotAndAnchors(this RectTransform trans, Vector2 aVec)
        {
            trans.pivot = aVec;
            trans.anchorMin = aVec;
            trans.anchorMax = aVec;
        }

        public static void SetSize(this RectTransform trans, Vector2 newSize)
        {
            var oldSize = trans.rect.size;
            var deltaSize = newSize - oldSize;
            trans.offsetMin -= new Vector2(deltaSize.x * trans.pivot.x, deltaSize.y * trans.pivot.y);
            trans.offsetMax += new Vector2(deltaSize.x * (1f - trans.pivot.x), deltaSize.y * (1f - trans.pivot.y));
        }

        public static void SetWidth(this RectTransform trans, float newSize)
        {
            SetSize(trans, new Vector2(newSize, trans.rect.size.y));
        }

        public static void SetHeight(this RectTransform trans, float newSize)
        {
            SetSize(trans, new Vector2(trans.rect.size.x, newSize));
        }

    }

}

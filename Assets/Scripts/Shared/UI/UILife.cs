using System.Collections.Generic;
using System.Linq;
using Assets.Resources.Data;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.Shared.UI
{
    public class UILife : MonoBehaviour
    {
        [SerializeField] private Image lifeImage;
        [SerializeField] private Transform lifeContainer;
        private MonkeySharedConfig _monkeySharedConfig;
        private readonly List<Image> lifes = new();

        [Inject]
        public void Constructor(MonkeySharedConfig monkeySharedConfig)
        {
            _monkeySharedConfig = monkeySharedConfig;
        }

        void Awake()
        {
            Reset();
        }

        public void Reset()
        {
            for (var i = lifeContainer.childCount - 1; i >= 0; i--)
            {
                Destroy(lifeContainer.GetChild(i).gameObject);
                lifes.Clear();
            }

            for (var x = 0; x != _monkeySharedConfig.life; x++)
            {
                var life = Instantiate(lifeImage, lifeContainer);
                life.enabled = true;
                lifes.Add(life);
            }

        }

        public void Decrease()
        {
            var lifeLast = lifes?.LastOrDefault(x => x.enabled);
            if (lifeLast != null) lifeLast.enabled = false;
        }

    }
}

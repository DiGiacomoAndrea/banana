﻿using UnityEngine.InputSystem;

namespace Assets.Scripts.Shared.UI
{
    public interface IUIManager
    {
        void ShowPause(InputAction.CallbackContext context);
        void ShowPause();
        void ShowGame();
        void ShowCredits();
        void ResetScene();
    }
}
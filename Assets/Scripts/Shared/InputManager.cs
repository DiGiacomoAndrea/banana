using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Scripts.Shared
{
    public class InputManager : MonoBehaviour
    {
        private InputAction _exitAction;

        void OnEnable()
        {
            var playerInput = GetComponent<PlayerInput>();
            _exitAction = playerInput.actions.FindAction("Exit");
        }

        void SetExitDisable(bool value)
        {
            if (value)
                _exitAction.Disable();
            else
                _exitAction.Enable();
        }

        public void MouseMove(InputAction.CallbackContext context)
        {
            Cursor.visible = true;
            //StopCoroutine("CursorHide");
            //StartCoroutine("CursorHide");
        }

        IEnumerator CursorHide()
        {
            yield return new WaitForSeconds(.5f);
            Cursor.visible = false;
        }

        void OnGUI()
        {
            //USERFULL
        }
    }
}



using Assets.Resources.Data;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Shared
{
    #region Types

    public class UIMatch
    {
        public bool IsOn;
        public GameObject GameObject { get; set; }

        public void SetOn()
        {
            IsOn = true;
            GameObject.GetComponent<Animator>().enabled = true;
            GameObject.transform.GetChild(0).gameObject.SetActive(true);
        }

        public void StopAnimation()
        {
            GameObject.GetComponent<Animator>().Rebind();;
            GameObject.GetComponent<Animator>().enabled = false;
        }
    }

    #endregion

    public class UIWin : MonoBehaviour
    {

        [SerializeField] private GameObject _uiMatch;
        private List<UIMatch> _uiMatches = new();
        private int _maxWin;


        [Inject]
        public void Constructor(MonkeySharedConfig monkeySharedConfig)
        {
            _maxWin = monkeySharedConfig.win;
        }


        void Awake()
        {
            RemoveChilds();
            AddChilds();
        }

        public void Increment()
        {
            var uiMatch = _uiMatches.FirstOrDefault(x => !x.IsOn);
            uiMatch?.SetOn();
        }

        private void AddChilds()
        {
            for (var i = 0; i < _maxWin; i++)
            {
                var uiMatch =Instantiate(_uiMatch, gameObject.transform);
                _uiMatches.Add(new UIMatch{ GameObject = uiMatch});
            }
        }

        void RemoveChilds()
        {
            _uiMatches = new List<UIMatch>();
            for (var i = transform.childCount - 1; i >= 0; i--)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }

        public void Hide()
        {
            _uiMatches.ForEach(x => x.StopAnimation());
        }

        public void Reset()
        {
            RemoveChilds();
            AddChilds();
        }
    }
}
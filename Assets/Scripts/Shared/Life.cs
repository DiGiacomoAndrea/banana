﻿using Assets.Resources.Data;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Shared
{
    public class Life
    {
        public int value { get; private set; }
        private int _maxLife;
        private readonly Animator _animator;
        private readonly AudioSource _audioSource;
        private readonly SignalBus _singnalBus;

        public Life(MonkeySharedConfig monkeySharedConfig, Animator animator, AudioSource audioSource, SignalBus signalBus)
        {
            _maxLife = monkeySharedConfig.life;
            value = _maxLife;
            _animator = animator;
            _audioSource = audioSource;
            _singnalBus = signalBus;
        }

        public void Decrease()
        {
            value--;
            _singnalBus.Fire(new LifeDecreaseSignal());
            _animator.SetBool("IsHit", true);
            _audioSource.Play();
        }

        public void Reset()
        {
            value = _maxLife;
            _singnalBus.Fire(new LifeResetSignal());
        }
    }

    public class LifeDecreaseSignal
    {
    }

    public class LifeResetSignal
    {
    }
}

using TMPro;
using UnityEngine;

namespace Assets.Scripts.Shared
{
    public class WinManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _winsPlayer1;
        [SerializeField] private TextMeshProUGUI _winsPlayer2;

        public void IncrementPlayer1() => Increment(_winsPlayer1);

        public void IncrementPlayer2() => Increment(_winsPlayer2);

        private void Increment(TextMeshProUGUI textMeshProUgui)
        {
            var wins = int.Parse(textMeshProUgui.text) + 1;
            if (wins <= 99)
                textMeshProUgui.text = wins.ToString();
        }

        public void Reset()
        {
            _winsPlayer1.text = "0";
            _winsPlayer2.text = "0";
        }
    }
}

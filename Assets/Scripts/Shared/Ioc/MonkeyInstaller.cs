using Assets.Scripts.Shared.UI;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Assets.Scripts.Shared.Ioc
{
    public class MonkeyInstaller : MonoInstaller
    {
        [SerializeField] private Rigidbody2D _Rigidbody2D;
        [SerializeField] private UIShoot uiShootStandard;
        [SerializeField] private UIShoot uiShootIce;
        [SerializeField] private UIStaminaStandard uiStaminaStandard;
        [SerializeField] private UIStaminaIce uiStaminaIce;
        [SerializeField] private UILifeNEW uiLife;
        [SerializeField] private UIReady uiReady;
        [SerializeField] private UIShoots uiShoots;
        [SerializeField] private Bullet _bulletStandard;
        [SerializeField] private Bullet _bulletIce;
        [SerializeField] private UnityEvent _onReadySignal;

        public override void InstallBindings()
        {
            //SIGNAL
            Container.DeclareSignal<StaminaRigenerateSignal>();
            Container.DeclareSignal<StaminaAddSignal>();
            Container.DeclareSignal<StaminaDecreaseSignal>();
            Container.DeclareSignal<StaminaResetSignal>();
            Container.DeclareSignal<ShootChangeSignal>();
            Container.DeclareSignal<ShootIceChangeSignal>();
            Container.DeclareSignal<LifeDecreaseSignal>();
            Container.DeclareSignal<LifeResetSignal>();
            Container.DeclareSignal<ReadyChangeSignal>();
            Container.DeclareSignal<ReadySignal>();
            Container.DeclareSignal<WeaponChangeSignal>();

            //WEAPON STANDARD UI
            Container.BindSignal<ShootChangeSignal>().ToMethod((x) => uiShootStandard.UpdateSlider(x.value));
            Container.BindSignal<ShootChangeSignal>().ToMethod((x) => uiStaminaStandard.SetStaminaShoot(x.value, x.hasShoot));
            Container.BindSignal<StaminaAddSignal>().ToMethod((x) => uiStaminaStandard.Add(x.value));
            Container.BindSignal<StaminaDecreaseSignal>().ToMethod((x) => uiStaminaStandard.Decrease(x.value));
            Container.BindSignal<StaminaResetSignal>().ToMethod(() => uiStaminaStandard.Reset());

            //WEAOPON ICE UI
            Container.BindSignal<ShootIceChangeSignal>().ToMethod((x) => uiShootIce.UpdateSlider(x.value));
            Container.BindSignal<ShootIceChangeSignal>().ToMethod((x) => uiStaminaIce.SetStaminaShoot(x.value, x.hasShoot));
            Container.BindSignal<StaminaAddSignal>().ToMethod((x) => uiStaminaIce.Add(x.value));
            Container.BindSignal<StaminaDecreaseSignal>().ToMethod((x) => uiStaminaIce.Decrease(x.value));
            Container.BindSignal<StaminaResetSignal>().ToMethod(() => uiStaminaIce.Reset());

            //WEAPON SWAP UI
            Container.BindSignal<WeaponChangeSignal>().ToMethod(x => uiShoots.Change(x.ShootTypeEnum));

            //LIFE UI
            Container.BindSignal<LifeDecreaseSignal>().ToMethod(() => uiLife.Decrease());
            Container.BindSignal<LifeResetSignal>().ToMethod(() => uiLife.Reset());

            //READY UI
            Container.BindSignal<ReadyChangeSignal>().ToMethod(x => uiReady.UpdateSlider(x.value));
            Container.BindSignal<ReadySignal>().ToMethod(() => _onReadySignal?.Invoke());

            //MONO BEHAVIOUR
            Container.Bind<Bullet>().WithId("bulletStandard").FromInstance(_bulletStandard);
            Container.Bind<Bullet>().WithId("bulletIce").FromInstance(_bulletIce);

            //CLASS
            Container.BindInterfacesAndSelfTo<Win>().AsSingle();
            Container.BindInterfacesAndSelfTo<Life>().AsSingle();
            Container.BindInterfacesAndSelfTo<Ready>().AsSingle();
            Container.BindInterfacesAndSelfTo<Move>().AsSingle();
            Container.BindInterfacesAndSelfTo<StaminaNew>().AsSingle();
            Container.BindInterfacesAndSelfTo<WeaponStandard>().AsSingle();
            Container.BindInterfacesAndSelfTo<WeaponIce>().AsSingle();
            Container.BindInterfacesAndSelfTo<WeaponHandler>().AsSingle();
        }
    }
}




using System.Collections.Generic;
using Assets.Scripts.Shared;
using Assets.Scripts.Shared.UI;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

public class SceneInstaller : MonoInstaller
{
    [SerializeField] private PlayerInput player1;
    [SerializeField] private PlayerInput player2;
    [SerializeField] private Animator _animatorGameEndPlayer1;
    [SerializeField] private Animator _animatorGameEndPlayer2;
    [SerializeField] private UIWin _UIWinPlayer1;
    [SerializeField] private UIWin _UIWinPlayer2;
    [SerializeField] private GameStateManager _gameStateManager;

    public override void InstallBindings()
    {
        Container.Bind<PlayerInput>().FromMethodMultiple(_ => new List<PlayerInput> { player1, player2 }).AsSingle();
        Container.Bind<Animator>().WithId("gameEndPlayer1").FromInstance(_animatorGameEndPlayer1);
        Container.Bind<Animator>().WithId("gameEndPlayer2").FromInstance(_animatorGameEndPlayer2).AsSingle();
        Container.Bind<UIWin>().WithId("UIWinPlayer1").FromInstance(_UIWinPlayer1);
        Container.Bind<UIWin>().WithId("UIWinPlayer2").FromInstance(_UIWinPlayer2).AsSingle();
        Container.Bind<GameEnd>().ToSelf().AsSingle();
        Container.Bind<GameStateManager>().FromInstance(_gameStateManager).AsSingle();

        SignalBusInstaller.Install(Container);
    }
}
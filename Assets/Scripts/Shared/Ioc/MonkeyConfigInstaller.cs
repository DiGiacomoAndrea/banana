using Assets.Resources.Data;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Shared.Ioc
{
    [CreateAssetMenu(fileName = "MonkeyConfigInstaller", menuName = "Installers/MonkeyConfigInstaller")]
    public class MonkeyConfigInstaller : ScriptableObjectInstaller<MonkeyConfigInstaller>
    {
        public MonkeyConfig MonkeyConfig;

        public override void InstallBindings()
        {
            Container.BindInstances(MonkeyConfig);
        }
    }
}
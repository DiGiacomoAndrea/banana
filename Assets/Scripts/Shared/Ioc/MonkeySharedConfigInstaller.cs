using Assets.Resources.Data;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Shared.Ioc
{
    [CreateAssetMenu(fileName = "MonkeySharedConfigInstaller", menuName = "Installers/MonkeySharedConfigInstaller")]
    public class MonkeySharedConfigInstaller : ScriptableObjectInstaller<MonkeySharedConfigInstaller>
    {
        public MonkeySharedConfig MonkeySharedConfig;

        public override void InstallBindings()
        {
            Container.BindInstances(MonkeySharedConfig);
        }
    }
}
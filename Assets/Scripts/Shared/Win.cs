﻿using Assets.Resources.Data;

namespace Assets.Scripts.Shared
{
    public class Win
    {
        public int LooseCount { get; private set; }
        private int _maxWin;

        public Win(MonkeySharedConfig monkeySharedConfig)
        {
            _maxWin = monkeySharedConfig.win;
            LooseCount = _maxWin;
        }

        public void Decrease()
        {
            LooseCount--;
        }

        public void Reset()
        {
            LooseCount = _maxWin;
        }
    }
}
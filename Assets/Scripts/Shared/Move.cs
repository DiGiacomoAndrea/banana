﻿using Assets.Resources.Data;
using UnityEngine;

namespace Assets.Scripts.Shared
{
    public interface IMove
    {
        void Execute(Vector2 vector2);
        void SetFreeze(bool value);
    }

    public class Move : IMove
    {

        private Vector2 _moveDirection;
        private bool _isFreeze;
        private readonly MonkeySharedConfig _monkeySharedConfig;
        private readonly Rigidbody2D _rigidbody;
        private readonly Animator _animator;
        private readonly Vector3 _initialPosition;
        private readonly Transform _transform;
        
        public Move(Rigidbody2D rigidbody, MonkeySharedConfig monkeySharedConfig, Animator animator, RectTransform transform)
        {
            _rigidbody = rigidbody;
            _monkeySharedConfig = monkeySharedConfig;
            _animator = animator;
            _initialPosition = transform.position;
            _transform = transform;
        }

        public void Execute(Vector2 vector2)
        {
            _moveDirection = vector2;
            _rigidbody.linearVelocity = vector2 * _monkeySharedConfig.moveSpeed;
        }

        public void ExecuteAnimation(bool perform)
        {
            if (perform && !_isFreeze) SetMoveAnim(true);
            if (!perform) SetMoveAnim(false);
        }

        public void SetFreeze(bool value)
        {
            _isFreeze = value;
            _animator.SetBool("IsFreeze", value);

            if (value) SetMoveAnim(false); 
            if (!value && _moveDirection != Vector2.zero) SetMoveAnim(true);

            _rigidbody.constraints = value ?
                RigidbodyConstraints2D.FreezeAll :
                RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
        }

        public void Reset()
        {
            _transform.localPosition = _initialPosition;
            _animator.Rebind();
        }

        public void SetMoveAnim(bool value)
        {
            _animator.SetBool("IsMove", value);
        }
    }
}

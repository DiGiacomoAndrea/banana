﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Assets.Resources.Data;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Shared
{
    #region Types

    public interface IWeapon
    {
        Bullet Bullet { get; set; }
        float currentShootValue { get; }

        event Action<ShootStateEnum> OnShoot;
        void FireStart();
        void FireStop();
        void FireReset();
        bool TryGetShoot(out Shoot shoot);
        bool DecreaseStamina(Shoot shoot);
        Vector2 GetVelocity(Shoot shoot);
        bool IsStrong(Shoot shoot);
    }

    public interface IChange
    {
        public float value { get; set; }
        public bool hasShoot { get; set; }
    }

    public class ShootChangeSignal : IChange
    {
        public float value { get; set; }
        public bool hasShoot { get; set; }
    }

    public class ShootIceChangeSignal : IChange
    {
        public float value { get; set; }
        public bool hasShoot { get; set; }
    }

    public class WeaponChangeSignal
    {
        public ShootTypeEnum ShootTypeEnum;
    }

    public enum ShootTypeEnum
    {
        Standard,
        Ice
    }

    public enum ShootStateEnum
    {
        Null,
        Start,
        Stop
    }

    #endregion

    public abstract class WeaponBase<T> : ITickable, IWeapon where T : IChange, new()
    {
        public event Action<ShootStateEnum> OnShoot;
        public float currentShootValue => _timeShoot.ElapsedMilliseconds;

        public Bullet Bullet { get; set; }

        private readonly IList<Shoot> _shoots;
        private readonly IStamina _stamina;
        private readonly int _direction;
        private readonly Stopwatch _timeShoot;
        private readonly SignalBus _signalBus;

        public WeaponBase(
            Bullet bullet,
            IList<Shoot> shoots,
            IStamina stamina,
            int direction,
            SignalBus signalBus
            )
        {
            Bullet = bullet;
            _stamina = stamina;
            _timeShoot = new Stopwatch();
            _signalBus = signalBus;
            _direction = direction;
            _shoots = shoots;
        }

        public void FireStart()
        {
            OnShoot?.Invoke(ShootStateEnum.Start);
            _timeShoot.Start();
        }

        public void FireStop()
        {
            OnShoot?.Invoke(ShootStateEnum.Stop);
            _timeShoot.Stop();
        }

        public void FireReset()
        {
            _signalBus.Fire(new T { value = 0 });
            _timeShoot.Reset();
        }

        public void Tick()
        {
            var shootType = _shoots.Get(currentShootValue);
            if (currentShootValue > 0)
                _signalBus.Fire(new T { value = currentShootValue, hasShoot = shootType.HasValue && _stamina.HasStamina(shootType.Value.Stamina) });
        }

        public bool DecreaseStamina(Shoot shoot)
        {
            return _stamina.Decrease(shoot.Stamina);
        }

        public Vector2 GetVelocity(Shoot shoot)
        {
            var velx = shoot.Speed * Mathf.Cos(shoot.Angle * _direction * Mathf.Deg2Rad);
            var vely = shoot.Speed * Mathf.Sin(shoot.Angle * _direction * Mathf.Deg2Rad);

            return new Vector2(velx, vely) * _direction;
        }

        public bool TryGetShoot(out Shoot shoot)
        {
            var shootType = _shoots.Get(currentShootValue);

            if (shootType.HasValue && _stamina.HasStamina(shootType.Value.Stamina))
            {
                shoot = shootType.Value;
                return true;
            }

            shoot = default;
            return false;
        }

        public virtual bool IsStrong(Shoot shoot)
        {
            return _shoots.Max(x => x) == shoot;
        }

        public void Pause(bool value)
        {
            _timeShoot.Reset();
        }

        public void Reset()
        {
            _timeShoot.Reset();
            _signalBus.Fire(new T { value = 0 });
        }


    }

    public class WeaponStandard : WeaponBase<ShootChangeSignal>
    {
        public WeaponStandard(
            [Inject(Id = "bulletStandard")] Bullet bullet,
            MonkeyConfig monkeyConfig,
            MonkeySharedConfig monkeySharedConfig,
            IStamina stamina,
            SignalBus signalBus
            ) : base(bullet, monkeySharedConfig.ShootsStardard, stamina, monkeyConfig.direction, signalBus)
        {
        }
    }

    public class WeaponIce : WeaponBase<ShootIceChangeSignal>
    {
        public WeaponIce(
            [Inject(Id = "bulletIce")] Bullet bullet,
            MonkeyConfig monkeyConfig,
            MonkeySharedConfig monkeySharedConfig,
            IStamina stamina,
            SignalBus signalBus) : base(bullet, monkeySharedConfig.ShootsIce, stamina, monkeyConfig.direction, signalBus)
        {
        }

        public override bool IsStrong(Shoot shoot) => true;
    }

    public class WeaponHandler : IWeaponsHandler, IInitializable, IDisposable
    {
        public event Action OnShootStart;
        public event Action OnShootEnd;

        private readonly WeaponStandard _weaponStandard;
        private readonly WeaponIce _weaponIce;
        private readonly SignalBus _signalBus;
        private List<IWeapon> _weapons;
        private IWeapon _selected;

        public WeaponHandler(
            WeaponStandard weaponStandard,
            WeaponIce weaponIce,
            SignalBus signalBus)
        {
            _weaponStandard = weaponStandard;
            _weaponIce = weaponIce;
            _signalBus = signalBus;
            _weapons = new List<IWeapon> { _weaponStandard, _weaponIce };
            _selected = _weaponStandard;
        }

        public void Initialize()
        {
            _weapons.ForEach(x => x.OnShoot += weapon_OnShootStart);
            _weapons.ForEach(x => x.OnShoot += weapon_OnShootEnd);
        }

        public void Dispose()
        {
            _weapons.ForEach(x => x.OnShoot -= weapon_OnShootStart);
            _weapons.ForEach(x => x.OnShoot -= weapon_OnShootEnd);
        }

        private void weapon_OnShootStart(ShootStateEnum obj)
        {
            if (obj == ShootStateEnum.Start)
                OnShootStart?.Invoke();
        }

        private void weapon_OnShootEnd(ShootStateEnum obj)
        {
            if (obj == ShootStateEnum.Stop)
                OnShootEnd?.Invoke();
        }

        public IWeapon GetCurrentWeapon() => _selected;

        public IWeapon ChangeWeapon()
        {
            _selected?.FireReset();
            _selected = _selected is WeaponStandard ? _weaponIce : _weaponStandard;
            _signalBus.Fire(new WeaponChangeSignal
            {
                ShootTypeEnum = _selected is WeaponStandard ? ShootTypeEnum.Standard : ShootTypeEnum.Ice
            });
            return _selected;
        }

        public void Pause(bool value)
        {
            _weaponStandard.Pause(value);
            _weaponIce.Pause(value);
        }

        public void Reset()
        {
            _selected = _weaponStandard;
            _signalBus.Fire(new WeaponChangeSignal { ShootTypeEnum = ShootTypeEnum.Standard });

            _weaponStandard.Reset();
            _weaponIce.Reset();
        }
    }

    public interface IWeaponsHandler
    {
        public event Action OnShootStart;
        public event Action OnShootEnd;

        public IWeapon GetCurrentWeapon();
        public IWeapon ChangeWeapon();

        public void Pause(bool value);

        public void Reset();
    }
}

using Assets.Scripts.Shared.UI;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Shared
{
    public class ReadyManager : MonoBehaviour
    {
        private IUIManager _uiManager;
        private readonly int _playerNumber = 2;
        private int _readySignalCount;

        [Inject]
        public void Constructor(IUIManager uiManager)
        {
            _uiManager = uiManager;
        }
        
        public void ReadyChangeSignalHandler()
        {
            _readySignalCount++;
            if (_readySignalCount == _playerNumber)
            {
                _uiManager.ResetScene();
                _readySignalCount = 0;
            }
        }

    }
}

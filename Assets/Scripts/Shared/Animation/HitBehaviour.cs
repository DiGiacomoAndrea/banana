using UnityEngine;

namespace Assets.Scripts.Shared.Animation
{
    public class HitBehaviour : StateMachineBehaviour
    {
        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.SetBool("IsHit", false);
        }
    }
}
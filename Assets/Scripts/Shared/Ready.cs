using System.Diagnostics;
using Zenject;

namespace Assets.Scripts.Shared
{
    public interface IReady
    {
        void ReadyStart();
        void ReadyReset();
    }

    public class Ready : ITickable
    {
        public int value => _timer.Elapsed.Milliseconds;

        private readonly SignalBus _signalBus;
        private readonly Stopwatch _timer;
        public bool IsReady;

        public Ready(SignalBus signalBus)
        {
            _signalBus = signalBus;
            _timer = new Stopwatch();
        }

        public void Start()
        {
            _timer.Start();
        }

        public void Reset()
        {
            _timer.Reset();
            _signalBus.Fire(new ReadyChangeSignal { value = 0 });
            IsReady = false;
        }

        public void Tick()
        {
            if (_timer.Elapsed.Milliseconds > 600 && !IsReady)
            {
                IsReady = true;
                _signalBus.Fire(new ReadySignal());
                _timer.Reset();
            }
        }
    }

    public class ReadyChangeSignal
    {
        public int value;
    }

    public class ReadySignal
    {
    }

}
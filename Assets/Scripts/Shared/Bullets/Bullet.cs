using Assets.Scripts.Shared.Bullets;
using UnityEngine;

namespace Assets.Scripts.Shared
{

    public class Bullet : MonoBehaviour, IBullet
    {
        protected BulletLogic BulletBase;

        public bool IsToSlow => BulletBase.IsToSlow;

        public bool IsSlow => BulletBase.IsSlow;

        public void EasterEgg() => BulletBase.EasterEgg();

        public bool GetIsToDestroy(Collision2D collider2D) => BulletBase.GetIsToDestroy(collider2D);

        public bool GetIsToSlow(Collision2D colliion2D) => BulletBase.GetIsToSlow(colliion2D);

        public void Launch(Vector2 shoot) => BulletBase.Launch(shoot);

        public void SetSlow() => BulletBase.SetSlow();

        public void SetStartingForce(Vector2 shoot) => BulletBase.SetStartingForce(shoot);

        public void ShowParticleIce() => BulletBase.ShowParticleIce();

        void Reset() => Destroy(gameObject);
    }

    public class BulletIce : Bullet
    {
    }
}
﻿using UnityEngine;
using Zenject;

namespace Assets.Scripts.Shared.Bullets
{
    public class BulletStandardLogic : BulletLogic
    {
        private readonly ParticleSystem _particleIce;
        private readonly ParticleSystem _particleFast;
        private Vector2 _shoot;

        public BulletStandardLogic(Rigidbody2D rigidbody2D,
            [Inject(Id = "ParticleFast")] ParticleSystem particleFast,
            [Inject(Id = "ParticleIce")] ParticleSystem particleIce) : base(rigidbody2D, particleIce)
        {
            _particleFast = particleFast;
            _particleIce = particleIce;
            _particleFast.Stop();
        }

        public override bool GetIsToSlow(Collision2D colliion2D)
        {
            if (colliion2D.gameObject.GetComponent<BulletIce>() != null)
                return true;

            return false;
        }

        public override void ShowParticleIce()
        {
            base.ShowParticleIce();
            _particleFast.gameObject.SetActive(false);
        }

        public void SetFastShootParticle(bool value)
        {
            if (!value) _particleFast.Stop();
        }

        public void SetIceParticle(bool value)
        {
            _particleIce.gameObject.SetActive(true);
            
        }
    }
}
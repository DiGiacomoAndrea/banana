using System;
using UnityEngine;

namespace Assets.Scripts.Shared.Bullets
{
    public abstract class BulletLogic : IBullet
    {
        public bool IsSlow { get; set; }

        public bool IsToSlow { get; set; }

        private Vector2 _shoot;
        private Rigidbody2D _rigidbody2D;
        private readonly ParticleSystem _particleIce;

        public BulletLogic(Rigidbody2D rigidbody2D, ParticleSystem particleIce)
        {
            _rigidbody2D = rigidbody2D;
            _particleIce = particleIce;
            _particleIce.Stop();
        }

        public void EasterEgg()
        {
            _rigidbody2D.linearVelocity = Vector2.zero;
        }

        public void Launch(Vector2 shoot)
        {
            _rigidbody2D.linearVelocity = shoot;
        }

        public virtual void ShowParticleIce()
        {
            _particleIce.gameObject.SetActive(true);
        }

        public virtual bool GetIsToDestroy(Collision2D collision2D)
        {
            var gameObjectName = collision2D.gameObject.name;
            if (gameObjectName.Contains("Monkey") || gameObjectName.Contains("BackGround"))
            {
                return true;
            }

            return false;
        }

        
        public abstract bool GetIsToSlow(Collision2D colliion2D);

        public void SetSlow()
        {
            IsSlow = true;

            const float SLOW_FACTOR = 0.2f;
            const float EPSILON = 0.0001f; 

            var currentVelocity = _rigidbody2D.linearVelocity;
            var currentAngularVelocity = _rigidbody2D.angularVelocity;

            var slowedVelocityX = MathF.Round(currentVelocity.x * SLOW_FACTOR, 4);
            var slowedVelocityY = MathF.Round(currentVelocity.y * SLOW_FACTOR, 4);

            _rigidbody2D.linearVelocity = new Vector2(
                Math.Abs(slowedVelocityX) < EPSILON ? 0f : slowedVelocityX,
                Math.Abs(slowedVelocityY) < EPSILON ? 0f : slowedVelocityY
            );

            var slowedAngularVelocity = MathF.Round(currentAngularVelocity * SLOW_FACTOR, 4);

            _rigidbody2D.angularVelocity = Math.Abs(slowedAngularVelocity) < EPSILON
                ? 0f
                : slowedAngularVelocity;

            _rigidbody2D.gravityScale = SLOW_FACTOR;
            _rigidbody2D.gravityScale = 0.2f;
        }

        public void SetStartingForce(Vector2 shoot)
        {
            _shoot = shoot;
        }

    }
}
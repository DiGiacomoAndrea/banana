﻿using UnityEngine;

namespace Assets.Scripts.Shared.Bullets
{
    public interface IBullet
    {
        bool IsToSlow { get; }
        bool IsSlow { get; }
        bool GetIsToSlow(Collision2D colliion2D);
        void SetStartingForce(Vector2 shoot);
        void SetSlow();
        void ShowParticleIce();
        void EasterEgg();
        bool GetIsToDestroy(Collision2D collider2D);
        void Launch(Vector2 shot);
    }
}
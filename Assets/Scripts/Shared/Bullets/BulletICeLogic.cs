﻿using UnityEngine;
using Zenject;

namespace Assets.Scripts.Shared.Bullets
{
    public class BulletICeLogic : BulletLogic
    {
        private readonly Rigidbody2D _rigidbody2D;
        private readonly ParticleSystem _particleIce;

        public BulletICeLogic(Rigidbody2D rigidbody2D,
            [Inject(Id = "ParticleIce")] ParticleSystem particleIce) : base(rigidbody2D, particleIce)
        {
            _rigidbody2D = rigidbody2D;
            _particleIce = particleIce;
            _particleIce.Play();
        }

        public override bool GetIsToSlow(Collision2D collision2D)
        {
            if (collision2D.gameObject.GetComponent<Bullet>() != null ||
                collision2D.gameObject.name.Contains("Helmet"))
                return true;

            return false;
        }
    }
}
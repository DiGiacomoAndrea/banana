﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;

namespace Assets.Scripts.Shared
{
    public class InputHelper
    {
        private static InputDevice Keyboard = InputSystem.devices.First(d => d is Keyboard);
        private static InputDevice Mouse = InputSystem.devices.First(d => d is Mouse);

        public static void PairDevice(PlayerInput playerInput)
        {
            var gamepads = InputUser.GetUnpairedInputDevices().Where(d => d is Gamepad);
            var keyboardAndMouse = InputSystem.devices.Where(d => d is Mouse || d is Keyboard).ToArray();

            if (gamepads.Any()) playerInput.SwitchCurrentControlScheme(Keyboard,Mouse, gamepads.First());
            else playerInput.SwitchCurrentControlScheme(keyboardAndMouse);
        }

        public static void DeviceChangeHandler(List<PlayerInput> playerInputs, InputDevice device, InputDeviceChange change)
        {
            if (device is Gamepad && change == InputDeviceChange.Added)
            {
                var firstPlayerInputWithoutGamepad = playerInputs.FirstOrDefault(x => !x.user.pairedDevices.Any(d => d is Gamepad));
                firstPlayerInputWithoutGamepad?.SwitchCurrentControlScheme(Keyboard, Mouse, device);
            }

            var playerLostGamepad = playerInputs.FirstOrDefault(x => x.user.lostDevices.FirstOrDefault(z => z == device) != null);

            if (device is Gamepad &&
                change == InputDeviceChange.Removed)
                playerLostGamepad?.SwitchCurrentControlScheme(Keyboard, Mouse);
        }

        public static void DeviceChangeHandler(PlayerInput playerInput, InputDevice device, InputDeviceChange change)
        {
            if (!playerInput.isActiveAndEnabled) return;

            var keyboardAndMouse = InputSystem.devices.Where(d => d is Mouse || d is Keyboard).ToArray();

            if (device is Gamepad &&
                change == InputDeviceChange.Added &&
                !playerInput.user.pairedDevices.Any(d => d is Gamepad))
            {
                playerInput.SwitchCurrentControlScheme(Keyboard, Mouse, device);
            }

            if (device is Gamepad &&
                change == InputDeviceChange.Removed)
                playerInput.SwitchCurrentControlScheme(keyboardAndMouse);
        }

        public static void PairPlayerInputGamepads(params PlayerInput[] playerInputs)
        {
            var gamepadsUnpaired = InputUser.GetUnpairedInputDevices().Where(d => d is Gamepad);
            var playerInputsCopy = new List<PlayerInput>(playerInputs);

            foreach (var gamepad in gamepadsUnpaired)
                foreach (var playerInput in playerInputsCopy)
                {
                    playerInputsCopy.Remove(playerInput);
                    playerInput.SwitchCurrentControlScheme(Keyboard, Mouse, gamepad);
                    break;
                }
        }

    }
}

using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Shared.Utils
{
    public class Timer : MonoBehaviour
    {
        [SerializeField] private int _count;
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private UnityEvent onTimerFinish;
        [SerializeField] private Animator _animator;

        void Start()
        {
            _text.text = _count.ToString();
        }
        
        public void StartCountdown()
        {
            gameObject.SetActive(true);
            _animator.SetBool("Run", true);
            _text.text = _count.ToString();
        }

        public void CountdownDecrement()
        {
            _text.text = (int.Parse(_text.text) - 1).ToString();
        }

        public void StopCountdown()
        {
            _animator.SetBool("Run", false);
            gameObject.SetActive(false);
            onTimerFinish.Invoke();
        }
    }
}

using UnityEngine;

namespace Assets.Scripts.Shared
{

    public class GameStateManager : MonoBehaviour
    {
        public bool IsGameOver { get; private set; }

        public void SetGameOver(bool state)
        {
            IsGameOver = state;
        }

        public void Reset()
        {
            IsGameOver = false;
        }
    }
}
﻿using Assets.Resources.Data;
using Zenject;

namespace Assets.Scripts.Shared
{
    public interface IStamina
    {
        bool HasStamina(float stamina);
        bool Decrease(float stamina);
        void Add(float stamina);
    }

    public class StaminaNew : IStamina
    {
        public float Max { get; }
        public float Current { get; private set; }

        private readonly SignalBus _signalBus;

        public StaminaNew(MonkeySharedConfig monkeySharedConfig, SignalBus signalBus)
        {
            Max = monkeySharedConfig.MaxStamina;
            Current = monkeySharedConfig.MaxStamina;
            _signalBus = signalBus;
        }

        public bool HasStamina(float stamina) => Current >= stamina;

        public void Add(float stamina)
        {
            Current += stamina;
            _signalBus.Fire(new StaminaAddSignal { value = Current });
        }

        public bool Decrease(float stamina)
        {
            if (Current >= stamina)
            {
                Current -= stamina;
                _signalBus.Fire(new StaminaDecreaseSignal { value = stamina });
                _signalBus.Fire(new StaminaRigenerateSignal());
                return true;
            }
            return false;
        }

        public void Reset()
        {
            Current = Max;
            _signalBus.Fire(new StaminaResetSignal());
        }
    }

    public class StaminaRigenerateSignal
    {
    }


    public class StaminaAddSignal
    {
        public float value;
    }

    public class StaminaDecreaseSignal
    {
        public float value;
    }

    public class StaminaResetSignal
    {
    }
}

using Assets.Scripts.Shared;
using Assets.Scripts.Shared.Bullets;
using FishNet.Object.Prediction;
using GameKit.Dependencies.Utilities;
using UnityEngine;

namespace Assets.Scripts.Multiplayer
{
    public class BulletIceNetwork : BulletIce
    {
        [SerializeField] private Rigidbody2D _rigidbody2D;
        [SerializeField] private ParticleSystem _particleIce;
        private BulletICeLogic _bulletIce;

        void Awake()
        {
            var predictionRigidbody2D = ObjectCaches<PredictionRigidbody2D>.Retrieve();
            predictionRigidbody2D.Initialize(_rigidbody2D);
            BulletBase = new BulletICeLogic(_rigidbody2D, _particleIce);
        }


        void OnCollisionEnter2D(Collision2D collision2D)
        {
            if (GetIsToDestroy(collision2D))
            {
                Destroy(gameObject);
            }

            if (GetIsToSlow(collision2D))
            {
                BulletBase.IsToSlow = true;
            }
        }

    }
}
using Assets.Scripts.Shared;
using Assets.Scripts.Shared.Bullets;
using FishNet.Object.Prediction;
using GameKit.Dependencies.Utilities;
using UnityEngine;

namespace Assets.Scripts.Multiplayer
{
    public class BulletStandardNetwork : Bullet
    {
        [SerializeField] private Rigidbody2D _rigidbody2D;
        [SerializeField] private ParticleSystem _particleIce;
        [SerializeField] private ParticleSystem _particleFast;
        private BulletStandardLogic _bulletStandard;

        void Awake()
        {
            var predictionRigidbody2D = ObjectCaches<PredictionRigidbody2D>.Retrieve();
            predictionRigidbody2D.Initialize(_rigidbody2D);

            BulletBase = new BulletStandardLogic(predictionRigidbody2D.Rigidbody2D, _particleFast, _particleIce);
        }

        void OnCollisionEnter2D(Collision2D collision2D)
        {
            if (GetIsToDestroy(collision2D))
            {
                Destroy(gameObject);
            }

            if (GetIsToSlow(collision2D))
            {
                BulletBase.IsToSlow = true;
                BulletBase.ShowParticleIce();
            }
        }

    }
}
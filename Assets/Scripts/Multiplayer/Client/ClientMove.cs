using FishNet.Object;
//using Unity.Netcode;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace Assets.Scripts.Multiplayer.Client
{
    public class ClientMove : NetworkBehaviour
    {
        //private readonly NetworkVariable<Vector3> _hostPosition = new();
        //private readonly NetworkVariable<Vector2> _clientMove =
        //    new(default, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);
        //private Vector2 _hostMove;
        //private Move _moveNew;

        //[Inject]
        //public void Construct(Move moveNew)
        //{
        //    _moveNew = moveNew;
        //    _clientMove.OnValueChanged += ClientMoveChanged;
        //}

        //private void ClientMoveChanged(Vector2 previousValue, Vector2 newValue)
        //{
        //    if (IsHost)
        //    {
        //        Debug.Log("Ricevuto movimento dal client");
        //        _hostMove = newValue;
        //    }
        //}

        public void OnMovement(InputAction.CallbackContext context)
        {
            Debug.Log("client move");

            if (!enabled) return;

            
            //_clientMove.Value = context.ReadValue<Vector2>();
            //_moveNew.ExecuteAnimation(context);
        }

        //public void FixedUpdate()
        //{
        //    if (IsHost)
        //    {
        //        _moveNew.Move(_hostMove);
        //        _hostPosition.Value = transform.localPosition;
        //    }

        //    if (IsClient && !IsHost)
        //    {
        //        _moveNew.Move(_clientMove.Value);
        //        transform.position = _hostPosition.Value;
        //    }
        //}

        //private void SetMoveAnim(bool value)
        //{
        //    _moveNew.SetFreeze(value);
        //}

        //void Pause(bool value)
        //{
        //    enabled = !value;
        //    if (!value) return;

        //    _hostMove = Vector2.zero;
        //    _clientMove.Value = Vector2.zero;
        //    _moveNew.SetMoveAnim(false);
        //}

        //void Reset()
        //{
        //    _clientMove.Value = Vector2.zero;
        //    _hostMove = Vector2.zero;
        //    _moveNew.Reset();
        //}
    }
}

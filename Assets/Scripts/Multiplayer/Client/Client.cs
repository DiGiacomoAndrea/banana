using Assets.Scripts.Multiplayer.UI;
using Assets.Scripts.Shared;
using FishNet.Object;
using FishNet.Transporting;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Scripts.Multiplayer.Client
{
    public class Client : NetworkBehaviour
    {
        [SerializeField] private PlayerInput _player1Input;
        [SerializeField] private PlayerInput _player2Input;
        [SerializeField] private UIManagerNetwork _uiManagerNetwork;
        private bool _wasClientOnly; //TO REVIEW why IsClientOnly not true?

        public override void OnStartClient()
        {
            if (NetworkManager.ClientManager.Clients.Count == 2)
            {
                _wasClientOnly = true;
                InputSystem.onDeviceChange += OnDeviceChangeHandler;
                NetworkManager.ClientManager.OnClientConnectionState += OnClientConnectionStateHandler;

                _player1Input.enabled = false;
                _player1Input.uiInputModule.DeactivateModule();

                _player2Input.ActivateInput();
                _player2Input.uiInputModule.ActivateModule();

                _uiManagerNetwork.JoinPlayer();

                Debug.Log("Client spawn");
            }
        }

        private void OnClientConnectionStateHandler(ClientConnectionStateArgs args)
        {
            switch (args.ConnectionState)
            {
                case LocalConnectionState.Stopped:
                    HandleClientDisconnect(args);
                    break;
            }
        }

        private void HandleClientDisconnect(ClientConnectionStateArgs args)
        {
            _uiManagerNetwork.Back();
        }

        public override void OnStopClient()
        {
            if (_wasClientOnly)
            {
                InputSystem.onDeviceChange -= OnDeviceChangeHandler;
            }
        }

        void OnDeviceChangeHandler(InputDevice device, InputDeviceChange change)
        {
            var playerInput = GetComponent<PlayerInput>();
            InputHelper.DeviceChangeHandler(playerInput, device, change);
        }
    }
}

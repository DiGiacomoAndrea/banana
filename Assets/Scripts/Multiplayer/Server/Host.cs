using Assets.Scripts.Multiplayer.UI;
using Assets.Scripts.Shared;
using FishNet.Managing;
using FishNet;
using FishNet.Object;
using UnityEngine;
using UnityEngine.InputSystem;
using FishNet.Connection;

namespace Assets.Scripts.Multiplayer.Server
{
    public class Host : NetworkBehaviour
    {
        [SerializeField] private GameObject player2;
        [SerializeField] private UIManagerNetwork _uiManagerNetwork;
        [SerializeField] private PlayerInput _playerInput1;
        [SerializeField] private PlayerInput _playerInput2;

        private NetworkManager _networkManager => InstanceFinder.NetworkManager;

        void Awake()
        {
            _playerInput1.DeactivateInput();
            _playerInput2.DeactivateInput();
        }

        public override void OnStartClient()
        {
            if (NetworkManager.ClientManager.Clients.Count == 1)
            {
                GiveOwnership(ClientManager.Connection);

                _playerInput2.enabled = false;
                _playerInput2.uiInputModule.DeactivateModule();

                _playerInput1.ActivateInput();
                _playerInput1.uiInputModule.ActivateModule();

                _uiManagerNetwork.JoinPlayer();

                Debug.Log("Host started");
            }
        }

        public override void OnStartServer()
        {
            _networkManager.ServerManager.OnRemoteConnectionState += ServerManager_OnRemoteConnectionState;
            InputSystem.onDeviceChange += OnDeviceChangeHandler;
        }

        public override void OnStopServer()
        {
            _networkManager.ServerManager.OnRemoteConnectionState -= ServerManager_OnRemoteConnectionState;
            InputSystem.onDeviceChange -= OnDeviceChangeHandler;
        }

        private void ServerManager_OnRemoteConnectionState(NetworkConnection arg1, FishNet.Transporting.RemoteConnectionStateArgs arg2)
        {
            if (arg2.ConnectionState == FishNet.Transporting.RemoteConnectionState.Started)
            {
                arg1.OnLoadedStartScenes += Arg1_OnLoadedStartScenes;
            }

            if (arg2.ConnectionState == FishNet.Transporting.RemoteConnectionState.Stopped)
            {
                player2.GetComponent<NetworkObject>().GiveOwnership(LocalConnection);
                arg1.OnLoadedStartScenes -= Arg1_OnLoadedStartScenes;
                // InstanceFinder.ServerManager.Despawn(player2);
                // _uiManagerNetwork.StopGame_ClientRpc();
                // Debug.Log("STop Game!!!");
            }
        }

        private void Arg1_OnLoadedStartScenes(NetworkConnection connection, bool arg2)
        {
            if (NetworkManager.ClientManager.Clients.Count == 2 && arg2)
            {
                Debug.Log("Client spawn");
                player2.GetComponent<NetworkObject>().GiveOwnership(connection);
                _uiManagerNetwork.StartGame_ClientRpc();
            }
        }

        private void OnDeviceChangeHandler(InputDevice device, InputDeviceChange change)
        {
            var playerInput = GetComponent<PlayerInput>();
            InputHelper.DeviceChangeHandler(playerInput, device, change);
        }

        private void OnGUI()
        {
            //USERFULL
        }
    }
}

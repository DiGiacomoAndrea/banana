using FishNet;
using System.Collections;
using System.Runtime.InteropServices;
using TMPro;
//using Unity.Netcode;
using UnityEngine;
using UnityEngine.EventSystems;

public class TextCopy : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] TextMeshProUGUI m_TextMeshPro;
    private string _joinCode;

    [DllImport("__Internal")]
    private static extern void TextCopyWeb(string text);

    void Awake()
    {
        _joinCode = PlayerPrefs.GetString("joinCode");
        m_TextMeshPro.text = _joinCode;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (InstanceFinder.IsHostStarted)
        {
            GUIUtility.systemCopyBuffer = _joinCode;
            StartCoroutine("ChangeTempText", "Copied");

#if UNITY_WEBGL
            TextCopyWeb(_joinCode);
#endif

        }
    }

    IEnumerator ChangeTempText(string value)
    {
        m_TextMeshPro.text = value;
        yield return new WaitForSeconds(0.5f);
        m_TextMeshPro.text = _joinCode;
    }
}

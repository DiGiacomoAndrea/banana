using Assets.Scripts.Multiplayer.UI;
using FishNet.Object;
using Zenject;

namespace Assets.Scripts.Multiplayer
{
    public class ReadyManagerNetwork : NetworkBehaviour
    {
        private UIManagerNetwork _uiManager;
        private readonly int _playerNumber = 2;
        private int _readySignalCount;

        [Inject]
        public void Constructor(UIManagerNetwork uiManager)
        {
            _uiManager = uiManager;
        }

        [ServerRpc(RunLocally = true)]
        public void ReadyChangeSignalHandler()
        {
            _readySignalCount++;
            if (_readySignalCount == _playerNumber)
            {
                _uiManager.ResetScene_ClientRpc();
                _readySignalCount = 0;
            }
        }

    }
}
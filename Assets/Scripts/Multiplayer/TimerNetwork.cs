using Assets.Scripts.Multiplayer.UI;
using FishNet;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using System;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Multiplayer
{
    public class TimerNetwork : NetworkBehaviour
    {
        [SerializeField] private int _count;
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private UIManagerNetwork _uiManagerNetwork;
        [SerializeField] private Animator _animator;

        private readonly SyncTimer _timeRemaining = new SyncTimer();

        //void Update()
        //{
        //    _timeRemaining.Update();
        //}

        public override void OnStartNetwork()
        {
            TimeManager.OnTick += TimeManager_OnTick;
        }

        public override void OnStopNetwork()
        {
            TimeManager.OnTick -= TimeManager_OnTick;
        }

        private void TimeManager_OnTick()
        {
            _timeRemaining.Update();
            _text.text = Mathf.CeilToInt(_timeRemaining.Remaining).ToString();
            Debug.Log(_timeRemaining.Remaining);
        }

        public void StartCountdown()
        {
            if (IsHostStarted)
            {
                _timeRemaining.StartTimer(_count, true);
                _text.text = ((int)_timeRemaining.Remaining).ToString();
                _animator.SetBool("Run", true);
            }
        }

        private void Awake()
        {
            _text.text = _count.ToString();
            _timeRemaining.OnChange += TimeRemaining_OnChange;
        }

        private void OnDestroy()
        {
            _timeRemaining.OnChange -= TimeRemaining_OnChange;
        }

        private void TimeRemaining_OnChange(SyncTimerOperation op, float prev, float next, bool asServer)
        {
            if (op == SyncTimerOperation.Start && IsClientOnlyStarted)
                _animator.SetBool("Run", true);

            //if (op == SyncTimerOperation.Finished
            //    && IsHostStarted)
            //    _uiManagerNetwork.StartGame_ClientRpc();
        }

        protected override void Reset()
        {
            base.Reset();
            _text.text = _count.ToString();
        }
    }
}

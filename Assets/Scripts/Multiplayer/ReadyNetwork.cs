using Assets.Scripts.Shared;
using FishNet.Object;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

public class ReadyNetwork : NetworkBehaviour, IReady
{
    private Ready _ready;
    private SignalBus _signalBus;

    [Inject]
    public void Constructor(Ready ready, SignalBus signalBus)
    {
        _signalBus = signalBus;
        _ready = ready;
    }

    void Update()
    {
        if (_ready.value > 0)
            _signalBus.Fire(new ReadyChangeSignal { value = _ready.value });
    }

    public void OnReady(InputAction.CallbackContext obj)
    {
        if (_ready.IsReady) return;

        if (obj.started && IsClientStarted) Start_ServerRpc();
        if (obj.started && IsHostStarted) Start_ObserverRpc();
        if (obj.canceled && IsClientStarted) Reset_ServerRpc();
        if (obj.canceled && IsHostStarted) Reset_ObeserverRpc();
    }

    public void ReadyStart() => _ready.Start();

    void IReady.ReadyReset() => _ready.Reset();

    [ObserversRpc(RunLocally = true)]
    private void Start_ObserverRpc() => ReadyStart();

    [ObserversRpc(RunLocally = true)]
    private void Reset_ObeserverRpc() => ReadyReset();

    [ServerRpc(RunLocally = true)]
    private void Start_ServerRpc() => ReadyStart();

    [ServerRpc(RunLocally = true)]
    private void Reset_ServerRpc() => ReadyReset();

    public void ReadyReset() => _ready.Reset();


}
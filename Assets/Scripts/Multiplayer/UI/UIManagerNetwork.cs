using System.Collections;
using System.Collections.Generic;
using Assets.Resources.Data;
using Assets.Scripts.Shared.UI;
using FishNet.Object;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Scripts.Multiplayer.UI
{
    public class UIManagerNetwork : NetworkBehaviour
    {
        [SerializeField] private Game _game;
        [SerializeField] private UIGameNetwork _uiGameNetwork;
        [SerializeField] private UIGameEnd _uiGameEndNetwork;
        [SerializeField] private UIPause _uiPause;
        [SerializeField] private UIGameOver _uiGameOver;
        private List<IScene> _uiGameScene;

        public void Awake()
        {
            _uiGameScene = new List<IScene>
            {
                _game.GetComponent<IScene>(),
                _uiGameNetwork.GetComponent<IScene>(),
                _uiGameEndNetwork.GetComponent<IScene>(),
                _uiPause.GetComponent<IScene>(),
                _uiGameOver.GetComponent<IScene>()
            };
        }

        [ObserversRpc]
        public void ResetScene_ClientRpc()
        {
            _uiGameScene.GetType<Game>()!.ResetScene();
            ShowGame(true);
            StartGame_ClientRpc();
        }

        public void JoinPlayer()
        {
            _uiGameScene.SetHides((list) =>
            {
                list.GetType<Game>().Show();
                list.GetType<UIGameNetwork>().Show();
                list.GetType<Game>().SetPause(true);
            });
        }

        public void ShowGame()
        {
            _uiGameScene.SetHides((list) =>
            {
                list.GetType<UIGameNetwork>().Show();
                list.GetType<Game>().SetPause(false);
                list.GetType<Game>().Show();
            });
        }

        [ObserversRpc(BufferLast = true)]
        public void StartGame_ClientRpc()
        {
            _uiGameNetwork.SetEnableCountdown(true);
            _uiGameNetwork.StartCountdown();
        }


        public void ShowPause(InputAction.CallbackContext context)
        {
            if (!context.performed) return;

            if (_uiPause.GetComponent<IScene>().IsVisible)
                ShowGame(false);
            else
                ShowPause();
        }

        private void ShowPause()
        {
            _uiGameScene.SetHides((list) =>
            {
                list.GetType<UIPause>().Show();
                list.GetType<Game>().Show();
                list.GetType<Game>().SetPause(true);
            });
        }

        private void ShowGame(bool isPause)
        {
            _uiGameScene.SetHides((list) =>
            {
                list.GetType<UIGameNetwork>().Show();
                list.GetType<Game>().SetPause(isPause);
                list.GetType<Game>().Show();
            });
        }


        public void Back()
        {
            if (IsClientStarted)
                NetworkManager.ClientManager.StopConnection();

            if (IsServerOnlyStarted)
            {
                NetworkManager.ServerManager.StopConnection(true);
                //Destroy(FindObjectOfType<DDOL>().gameObject);
            }

            Destroy(NetworkManager.gameObject);
            UnityEngine.SceneManagement.SceneManager.LoadScene("Home");

        }

        [ObserversRpc]
        public void ShowGameEnd(PlayerEnum loserPlayer)
        {
            _uiGameScene.SetHides((list) =>
            {
                list.GetType<Game>().SetPause(true);
                list.GetType<UIGameEnd>().Show(loserPlayer);
                list.GetType<Game>().Show();
            });
        }

        [ObserversRpc]
        public void ShowGameOver(PlayerEnum loserPlayer)
        {
            _uiGameScene.SetHides((list) =>
            {
                list.GetType<Game>().SetGameEnd();
                list.GetType<Game>().Show();
                list.GetType<UIGameOver>().Show(loserPlayer);
            });
        }
    }
}
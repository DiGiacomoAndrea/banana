﻿using System.Collections.Generic;
using System.Linq;
using Assets.Resources.Data;
using Assets.Scripts.Shared;
using Assets.Scripts.Shared.UI;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace Assets.Scripts.Multiplayer.UI
{
    public class UIGameOverNetwork : Scene
    {
        [SerializeField] private WinManager _winManager;
        [SerializeField] private List<Animator> _animatorsJoinPlayer1;
        [SerializeField] private List<Animator> _animatorsJoinPlayer2;
        [SerializeField] private List<Animator> _animatorsWinPlayer1;
        [SerializeField] private List<Animator> _animatorsWinPlayer2;

        private IEnumerable<PlayerInput> _playerInputs;
        private IEnumerable<ReadyNetwork> _readies;

        [Inject]
        public void Constructor(IEnumerable<PlayerInput> playerInputs, IEnumerable<ReadyNetwork> readies)
        {
            _playerInputs = playerInputs;
            _readies = readies;
        }

        public void Show(PlayerEnum looserPlayer)
        {
            base.Show();

            //switch control schema!
            if (_playerInputs.ElementAt(0).inputIsActive)
                _playerInputs.ElementAt(0).SwitchCurrentActionMap("Player1 GameOver");
            if (_playerInputs.ElementAt(1).inputIsActive)
                _playerInputs.ElementAt(1).SwitchCurrentActionMap("Player1 GameOver");

            if (looserPlayer == PlayerEnum.player2)
            {
                _animatorsWinPlayer1.ForEach(_ => _.SetFloat("Direction", 1));
                _winManager.IncrementPlayer1();
            }

            if (looserPlayer == PlayerEnum.player1)
            {
                _animatorsWinPlayer2.ForEach(_ => _.SetFloat("Direction", 1));
                _winManager.IncrementPlayer2();
            }

        }

        public void ShowPlayer1()
        {
            base.Show();

            _animatorsJoinPlayer1.ForEach(_ => _.SetFloat("Direction", 1));
            _animatorsJoinPlayer2.ForEach(_ => _.SetFloat("Direction", 1));
            _playerInputs.ElementAt(0).SwitchCurrentActionMap("Player1 GameOver");
        }

        public void ShowPlayer2()
        {
            base.Show();

            _animatorsJoinPlayer1.ForEach(_ => _.SetFloat("Direction", 1));
            _animatorsJoinPlayer2.ForEach(_ => _.SetFloat("Direction", 1));
            _playerInputs.ElementAt(1).SwitchCurrentActionMap("Player1 GameOver");
        }

        public override void Hide()
        {
            //:(
            if (_playerInputs.ElementAt(0).inputIsActive)
                _playerInputs.ElementAt(0).SwitchCurrentActionMap("Player1");
            if (_playerInputs.ElementAt(1).inputIsActive)
                _playerInputs.ElementAt(1).SwitchCurrentActionMap("Player1");

            _readies.ToList().ForEach(x => x.ReadyReset());

            _animatorsWinPlayer1
                .Concat(_animatorsWinPlayer2)
                .ToList()
                .ForEach(_ => _.Rebind());

            base.Hide();
        }
    }
}
﻿using Assets.Scripts.Shared.UI;
using Assets.Scripts.Shared.Utils;
using UnityEngine;

namespace Assets.Scripts.Multiplayer.UI
{
    public class UIGameNetwork : Scene
    {
        [SerializeField] private Timer _countdown;

        public override void Show()
        {
            gameObject.GetComponent<Canvas>().enabled = true;
            IsVisible = true;
        }

        public override void Hide()
        {
            gameObject.GetComponent<Canvas>().enabled = false;
            IsVisible = false;
        }

        public void StartCountdown()
        {
            _countdown.StartCountdown();
        }

        public void SetEnableCountdown(bool value)
        {
            _countdown.gameObject.SetActive(value);
        }
    }
} 
using Assets.Scripts.Multiplayer.UI;
using System.Collections.Generic;
using Assets.Scripts.Multiplayer;
using UnityEngine;
using Assets.Scripts.Shared;

public class MultiSceneInstaller : SceneInstaller
{
    [SerializeField] private ReadyNetwork _ready1;
    [SerializeField] private ReadyNetwork _ready2;

    public override void InstallBindings()
    {
        base.InstallBindings();

        Container.Bind<UIManagerNetwork>().FromComponentOn(gameObject).AsSingle();
        Container.Bind<ReadyManagerNetwork>().FromComponentOn(gameObject).AsSingle();
        Container.Bind<IReady>().FromMethodMultiple(_ => new List<ReadyNetwork> { _ready1, _ready2 }).AsSingle();
    }
}

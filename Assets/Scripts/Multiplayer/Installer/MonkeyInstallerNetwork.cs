using Assets.Scripts.Shared.Ioc;
using FishNet.Object.Prediction;
using GameKit.Dependencies.Utilities;
using UnityEngine;

namespace Assets.Scripts.Multiplayer.Installer
{
    public class MonkeyInstallerNetwork : MonkeyInstaller
    {
        [SerializeField] private BulletStandardNetwork bulletStandardPrefab;

        public override void InstallBindings()
        {
            base.InstallBindings();

            var predictionRigidbody2D = ObjectCaches<PredictionRigidbody2D>.Retrieve();
            predictionRigidbody2D.Initialize(GetComponent<Rigidbody2D>());

            Container.Bind<Rigidbody2D>().FromInstance(predictionRigidbody2D.Rigidbody2D).AsSingle();
        }
    }
}




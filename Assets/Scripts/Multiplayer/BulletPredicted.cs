using Assets.Scripts.Shared;
using Assets.Scripts.Shared.Bullets;
using FishNet.Connection;
using FishNet.Object;
using FishNet.Object.Prediction;
using FishNet.Serializing;
using FishNet.Transporting;
using GameKit.Dependencies.Utilities;
using UnityEngine;

namespace Assets.Scripts.Multiplayer
{
    public class BulletPredicted : NetworkBehaviour
    {
        //Replicate structure.
        public struct ReplicateData : IReplicateData
        {
            public bool IsToSlow;
            public bool IsSlow;

            //The uint isn't used but Unity C# version does not
            //allow parameter-less constructors we something
            //must be set as a parameter.
            public ReplicateData(bool isToFreeze, bool isSlow) : this()
            {
                IsToSlow = isToFreeze;
                IsSlow = isSlow;
            }
            private uint _tick;
            public void Dispose() { }
            public uint GetTick() => _tick;
            public void SetTick(uint value) => _tick = value;
        }

        //Reconcile structure.
        public struct ReconcileData : IReconcileData
        {
            public PredictionRigidbody2D PredictionRigidbody2D;

            public ReconcileData(PredictionRigidbody2D pr) : this()
            {
                PredictionRigidbody2D = pr;
            }

            private uint _tick;
            public void Dispose() { }
            public uint GetTick() => _tick;
            public void SetTick(uint value) => _tick = value;
        }

        //Forces are not applied in this example but you
        //could definitely still apply forces to the PredictionRigidbody2D
        //even with no controller, such as if you wanted to bump it
        //with a player.
        private PredictionRigidbody2D PredictionRigidbody2D;
        private Vector2 _shoot;
        private Bullet _bullet;
        private bool _isToSlow;
        private bool _isSlow;

        void Awake()
        {
            _bullet = GetComponent<Bullet>();
            PredictionRigidbody2D = ObjectCaches<PredictionRigidbody2D>.Retrieve();
            PredictionRigidbody2D.Initialize(GetComponent<Rigidbody2D>());
        }

        public override void WritePayload(NetworkConnection connection, Writer writer)
        {
            writer.WriteString($"My Id is {base.ObjectId}. Is PredictedSpawned? {base.NetworkObject.PredictedSpawner.IsValid}.");
            writer.WriteVector3(_shoot);
        }

        public override void ReadPayload(NetworkConnection connection, Reader reader)
        {
            var r = reader.ReadString();
            var shoot = reader.ReadVector3();
            SetStartingForce(shoot);
        }

        public override void OnStartNetwork()
        {
            _bullet.Launch(_shoot);
            base.TimeManager.OnTick += TimeManager_OnTick;
            base.TimeManager.OnPostTick += TimeManager_OnPostTick;
        }

        public override void OnStopNetwork()
        {
            base.TimeManager.OnTick -= TimeManager_OnTick;
            base.TimeManager.OnPostTick -= TimeManager_OnPostTick;
        }

        void Update()
        {
            if (IsOwner)
            {
                _isToSlow = _bullet.IsToSlow;
                _isSlow = _bullet.IsSlow;
            }
        }

        private void TimeManager_OnTick()
        {
            RunMove(CreateReplicateData());
        }


        private void TimeManager_OnPostTick()
        {
            CreateReconcile();
        }

        public void SetStartingForce(Vector2 shoot)
        {
            _shoot = shoot;
        }

        [Replicate]
        private void RunMove(ReplicateData md, ReplicateState state = ReplicateState.Invalid, Channel channel = Channel.Unreliable)
        {
            if (md.IsToSlow && !md.IsSlow)
                _bullet.SetSlow();

            PredictionRigidbody2D.Simulate();
        }

        //Create the reconcile data here and call your reconcile method.
        public override void CreateReconcile()
        {
            ReconcileData rd = new ReconcileData(PredictionRigidbody2D);
            ReconcileState(rd);
        }

        [Reconcile]
        private void ReconcileState(ReconcileData data, Channel channel = Channel.Unreliable)
        {
            //Call reconcile on your PredictionRigidbody2D field passing in
            //values from data.
            PredictionRigidbody2D.Reconcile(data.PredictionRigidbody2D);
        }

        private ReplicateData CreateReplicateData()
        {
            if (!base.IsOwner)
            {
                return default;
            }

            ReplicateData md = new ReplicateData(_isToSlow, _isSlow);

            return md;
        }


        //BUG FISHNET 3.11.2 OnChange syncvar is not call on predicted spawn
        //SyncVar to set spawn force. This is set by predicted spawner and sent to the server.
        // [SyncVar(OnChange = nameof(_startingForce_OnChange))]
        // private Vector2 _startingForce;

        // public void SetStartingForce(Vector2 value)
        // {
        //     // if (!IsSpawned)
        //     //     SetVelocity(value);
        //     // _startingForce = value;
        // }

    }
}
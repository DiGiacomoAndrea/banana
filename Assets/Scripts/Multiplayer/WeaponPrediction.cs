
using FishNet.Object;
using Assets.Resources.Data;
using Assets.Scripts.Shared;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace Assets.Scripts.Multiplayer
{
    public class WeaponPrediction : NetworkBehaviour
    {

        [SerializeField] private Transform _weaponPoint;
        private IWeaponsHandler _weaponsHandler;
        private bool _isShoot;
        private bool _subscribed;
        private Shoot _shoot;
        private IWeapon _selectedWeapon;
        private GameObject _selectedBullet;

        [Inject]
        public void Constructor(IWeaponsHandler weaponsHandler)
        {
            _weaponsHandler = weaponsHandler;
        }

        public override void OnStartClient()
        {
            base.OnStartClient();
            SubscribeToTimeManager(true);

            _selectedWeapon = _weaponsHandler.GetCurrentWeapon();
        }

        public override void OnStartServer()
        {
            base.OnStartServer();
            SubscribeToTimeManager(true);
        }

        private void SubscribeToTimeManager(bool subscribe)
        {
            if (TimeManager == null)
                return;

            if (subscribe == _subscribed)
                return;
            _subscribed = subscribe;

            if (subscribe)
            {
                TimeManager.OnTick += TimeManager_OnTick;
            }
            else
            {
                TimeManager.OnTick -= TimeManager_OnTick;
            }
        }

        private void TimeManager_OnTick()
        {
            if (IsOwner)
            {
                TryToSpawnBullet();
            }
        }

        public void OnChange(InputAction.CallbackContext context)
        {
            if (!enabled) return;

            if (!context.performed) return;

            if (IsHostStarted) ChangeWeapon_ObserversRpc();
            if (IsClientOnlyStarted) ChangeWeapon_ServerRpc();

        }

        [ServerRpc(RunLocally = true)]
        private void ChangeWeapon_ServerRpc() => OnChangeInternal();

        [ObserversRpc]
        private void ChangeWeapon_ObserversRpc() => OnChangeInternal();

        private void OnChangeInternal()
        {
            var weapon = _weaponsHandler.ChangeWeapon();

            _selectedWeapon = weapon;
        }

        private void TryToSpawnBullet()
        {
            if (_isShoot)
            {
                _isShoot = false;

                var nob = Instantiate(_selectedWeapon.Bullet, _weaponPoint.position, _weaponPoint.rotation);
                var bt = nob.GetComponent<BulletPredicted>();

                //BUG FISHNET 3.11.2 OnChange syncvar is not call on predicted spawn 
                bt.SetStartingForce(_selectedWeapon.GetVelocity(_shoot));

                Spawn(nob.gameObject, base.Owner);

                _shoot = new Shoot();
            }
        }

        public void OnFire(InputAction.CallbackContext context)
        {
            if (!enabled) return;

            if (context.started)
            {
                if (IsHostStarted) FireStart_ObserversRpc();
                if (IsClientOnlyStarted) FireStart_ServerRpc();
            }

            if (context.canceled)
            {
                if (IsHostStarted) FireStop_ObserversRpc();
                if (IsClientOnlyStarted) FireStop_ServerRpc();

                if (_selectedWeapon.TryGetShoot(out var shoot))
                {
                    _isShoot = true;
                    _shoot = shoot;

                    if (IsHostStarted) DecreaseStamina_ObserversRpc(shoot);
                    if (IsClientOnlyStarted) DecreaseStamina_ServerRpc(shoot);
                }

                if (IsHostStarted) FireReset_ObserversRpc();
                if (IsClientOnlyStarted) FireReset_ServerRpc();
            }
        }

        [ServerRpc(RunLocally = true)]
        private void DecreaseStamina_ServerRpc(Shoot shoot) => _selectedWeapon.DecreaseStamina(shoot);

        [ObserversRpc]
        private void DecreaseStamina_ObserversRpc(Shoot shoot) => _selectedWeapon.DecreaseStamina(shoot);

        [ServerRpc(RunLocally = true)]
        void FireStart_ServerRpc() => _selectedWeapon.FireStart();

        [ObserversRpc]
        void FireStart_ObserversRpc() => _selectedWeapon.FireStart();

        [ServerRpc(RunLocally = true)]
        void FireStop_ServerRpc() => _selectedWeapon.FireStop();

        [ObserversRpc]
        void FireStop_ObserversRpc() => _selectedWeapon.FireStop();

        [ServerRpc(RunLocally = true)]
        void FireReset_ServerRpc() => _selectedWeapon.FireReset();

        [ObserversRpc]
        void FireReset_ObserversRpc() => _selectedWeapon.FireReset();

        void Pause(bool value)
        {
            enabled = !value;
            _weaponsHandler.Pause(value);
        }

        protected override void Reset()
        {
            base.Reset();
            _weaponsHandler.Reset();
        }
    }
}
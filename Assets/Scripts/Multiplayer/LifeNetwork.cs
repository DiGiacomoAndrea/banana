using Assets.Resources.Data;
using Assets.Scripts.Shared;
using FishNet.Object;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Assets.Scripts.Multiplayer
{
    public class LifeNetwork : NetworkBehaviour
    {
        [SerializeField] private UnityEvent<PlayerEnum> _onGameEnd;
        [SerializeField] private UnityEvent<PlayerEnum> _onGameOver;
        private MonkeyConfig _monkeyConfig;
        private Win _win;
        private Life _life;
        private GameStateManager _gameStateManager;

        [Inject]
        public void Construnctor(Life life, Win win, MonkeyConfig monkeyConfig, GameStateManager gameStateManager)
        {
            _monkeyConfig = monkeyConfig;
            _win = win;
            _life = life;
            _gameStateManager = gameStateManager;
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            if (_gameStateManager.IsGameOver) return;

            if (IsHostStarted && collision.gameObject.name.Contains("Banana"))
            {
                Decrease_ObserverRpc();
            }
        }

        [ObserversRpc]
        void Decrease_ObserverRpc()
        {
            _life.Decrease();

            if (_life.value == 0)
            {
                _gameStateManager.SetGameOver(true);

                _win.Decrease();

                if (_win.LooseCount == 0)
                    _onGameOver.Invoke(_monkeyConfig.player);
                else
                    _onGameEnd.Invoke(_monkeyConfig.player);
            }
        }

        protected override void Reset()
        {
            base.Reset();
            _gameStateManager.Reset();
            _life.Reset();
        }
    }
}

using Assets.Scripts.Shared;
using FishNet.Object;
using FishNet.Object.Prediction;
using FishNet.Transporting;
using GameKit.Dependencies.Utilities;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;
using Vector2 = UnityEngine.Vector2;

public class MovePrediction : NetworkBehaviour
{

    public struct ReplicateData : IReplicateData
    {
        public Vector2 MoveDirection;
        public bool IsToAnimate;
        public bool IsToFreeze;

        public ReplicateData(Vector2 moveDirection, bool isToFreeze, bool isToAnimate) : this()
        {
            MoveDirection = moveDirection;
            IsToFreeze = isToFreeze;
            IsToAnimate = isToAnimate;
        }

        private uint _tick;
        public void Dispose() { }
        public uint GetTick() => _tick;
        public void SetTick(uint value) => _tick = value;
    }

    public struct ReconcileData : IReconcileData
    {
        //PredictionRigidbody is used to synchronize rigidbody states
        //and forces. This could be done manually but the PredictionRigidbody
        //type makes this process considerably easier. Velocities, kinematic state,
        //transform properties, pending velocities and more are automatically
        //handled with PredictionRigidbody.
        public PredictionRigidbody2D PredictionRigidbody2D;

        public ReconcileData(PredictionRigidbody2D pr) : this()
        {
            PredictionRigidbody2D = pr;
        }

        private uint _tick;
        public void Dispose() { }
        public uint GetTick() => _tick;
        public void SetTick(uint value) => _tick = value;
    }

    private Move _move;
    private IWeaponsHandler _weaponHandler;
    private ShootStateEnum _shootState;
    private Vector2 _moveDirection;
    private bool _isToAnimate;
    public PredictionRigidbody2D PredictionRigidbody2D;
    private bool _isToFreeze;

    private void Awake()
    {
        PredictionRigidbody2D = ObjectCaches<PredictionRigidbody2D>.Retrieve();
        PredictionRigidbody2D.Initialize(GetComponent<Rigidbody2D>());
    }

    [Inject]
    public void Construnctor(Move move, IWeaponsHandler weaponHandler)
    {
        _move = move;
        _weaponHandler = weaponHandler;
        _weaponHandler.OnShootStart += WeaponHandler_OnShootStart;
        _weaponHandler.OnShootEnd += WeaponHandler_OnShootEnd;
    }

    private void WeaponHandler_OnShootStart()
    {
        _isToFreeze = true;
    }

    private void WeaponHandler_OnShootEnd()
    {
        _isToFreeze = false;
    }

    public void OnMovement(InputAction.CallbackContext context)
    {
        if (!enabled) return;

        _moveDirection = context.ReadValue<Vector2>();
        _isToAnimate = context.performed;
    }

    private void OnDestroy()
    {
        ObjectCaches<PredictionRigidbody2D>.StoreAndDefault(ref PredictionRigidbody2D);
        _weaponHandler.OnShootStart += WeaponHandler_OnShootStart;
        _weaponHandler.OnShootEnd += WeaponHandler_OnShootEnd;

    }

    public override void OnStartNetwork()
    {
        base.TimeManager.OnTick += TimeManager_OnTick;
        base.TimeManager.OnPostTick += TimeManager_OnPostTick;
    }

    public override void OnStopNetwork()
    {
        base.TimeManager.OnTick -= TimeManager_OnTick;
        base.TimeManager.OnPostTick -= TimeManager_OnPostTick;
    }

    /* OnTick is the equivalent to FixedUpdate. OnTick is called
     * right before physics are updated. */
    private void TimeManager_OnTick()
    {
        RunMove(CreateReplicateData());
    }

    [Replicate]
    private void RunMove(ReplicateData moveData, ReplicateState state = ReplicateState.Invalid, Channel channel = Channel.Unreliable)
    {
        if (moveData.IsToFreeze)
            _move.SetFreeze(true);
        else
            _move.SetFreeze(false);

        _move.Execute(moveData.MoveDirection.normalized);
        _move.ExecuteAnimation(moveData.IsToAnimate);

        PredictionRigidbody2D.Simulate();
    }

    private ReplicateData CreateReplicateData()
    {
        if (!base.IsOwner)
        {
            return default;
        }

        ReplicateData md = new ReplicateData(_moveDirection, _isToFreeze, _isToAnimate);

        return md;
    }

    private void TimeManager_OnPostTick()
    {
        CreateReconcile();
    }

    //Create the reconcile data here and call your reconcile method.
    public override void CreateReconcile()
    {
        //We must send back the state of the rigidbody. Using your
        //PredictionRigidbody field in the reconcile data is an easy
        //way to accomplish this. More advanced states may require other
        //values to be sent; this will be covered later on.
        ReconcileData rd = new ReconcileData(PredictionRigidbody2D);
        //Like with the replicate you could specify a channel here, though
        //it's unlikely you ever would with a reconcile.
        ReconcileState(rd);
    }


    [Reconcile]
    private void ReconcileState(ReconcileData data, Channel channel = Channel.Unreliable)
    {
        //Call reconcile on your PredictionRigidbody field passing in
        //values from data.
        PredictionRigidbody2D.Reconcile(data.PredictionRigidbody2D);
    }

    void Pause(bool value)
    {
        if (value)
        {
            _moveDirection = Vector2.zero;
            _isToAnimate = false;
        }

        enabled = !value;
    }

    protected override void Reset()
    {
        _moveDirection = Vector2.zero;
        _move.Reset();
    }
}
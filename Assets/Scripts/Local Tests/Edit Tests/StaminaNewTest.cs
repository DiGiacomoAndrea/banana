using System.Linq;
using Zenject;
using NUnit.Framework;
using Assets.Resources.Data;
using Assets.Scripts.Shared;

[TestFixture]
public class StaminaNewTest : ZenjectUnitTestFixture
{
    private bool _staminaRigenerateSignalCalled;

    [SetUp]
    public override void Setup()
    {
        base.Setup();

        SignalBusInstaller.Install(Container);

        Container.DeclareSignal<StaminaRigenerateSignal>();
        Container.DeclareSignal<StaminaAddSignal>().OptionalSubscriber();
        Container.DeclareSignal<StaminaDecreaseSignal>().OptionalSubscriber();
        Container.DeclareSignal<StaminaResetSignal>().OptionalSubscriber();

        Container.Bind<MonkeySharedConfig>().FromScriptableObjectResource("Data/MonkeySharedConfig").AsSingle();
        Container.Bind<StaminaNew>().AsSingle();

        var signalBus = Container.Resolve<SignalBus>();

        signalBus.Subscribe<StaminaRigenerateSignal>(() => _staminaRigenerateSignalCalled = true);
    }

    [TearDown]
    public override void Teardown()
    {
        _staminaRigenerateSignalCalled = false;

        base.Teardown();
    }

    [Test]
    public void Stamina_Decrease_Signals()
    {
        var staminaNew = Container.Resolve<StaminaNew>();
        var shootsConfig = Container.Resolve<MonkeySharedConfig>();
        var fast = shootsConfig.ShootsStardard.Min(x => x.Stamina);

        _ = staminaNew.Decrease(fast);

        Assert.AreEqual(true, _staminaRigenerateSignalCalled);
    }

    [Test]
    public void Stamina_Decrease()
    {
        var staminaNew = Container.Resolve<StaminaNew>();
        var shootsConfig = Container.Resolve<MonkeySharedConfig>();
        var fast = shootsConfig.ShootsStardard.Min(x => x.Stamina);

        var actual = staminaNew.Decrease(fast);

        Assert.AreEqual(true, actual);
    }

    [Test]
    public void Stamina_Not_Decrease()
    {
        var staminaNew = Container.Resolve<StaminaNew>();
        var shootsConfig = Container.Resolve<MonkeySharedConfig>();
        var strong = shootsConfig.ShootsStardard.Max(x => x.Stamina);

        var actual = staminaNew.Decrease(strong + 1);

        Assert.AreEqual(false, actual);
    }

    [Test]
    public void Stamina_Has_Stamina()
    {
        var staminaNew = Container.Resolve<StaminaNew>();
        var shootsConfig = Container.Resolve<MonkeySharedConfig>();
        var fast = shootsConfig.ShootsStardard.Min(x => x.Stamina);

        var actual = staminaNew.HasStamina(fast);

        Assert.AreEqual(true, actual);
    }

    [Test]
    public void Stamina_Has_Not_Stamina()
    {
        var staminaNew = Container.Resolve<StaminaNew>();
        var shootsConfig = Container.Resolve<MonkeySharedConfig>();
        var strong = shootsConfig.ShootsStardard.Max(x => x.Stamina);

        var actual = staminaNew.HasStamina(strong + 1);

        Assert.AreEqual(false, actual);
    }

    [Test]
    public void Stamina_Add()
    {
        var staminaNew = Container.Resolve<StaminaNew>();
        var expected = staminaNew.Current + 10;

        staminaNew.Add(10);

        Assert.AreEqual(expected, staminaNew.Current);
    }

    [Test]
    public void Stamina_Reset()
    {
        var staminaNew = Container.Resolve<StaminaNew>();
        var shootsConfig = Container.Resolve<MonkeySharedConfig>();
        var strong = shootsConfig.ShootsStardard.Max(x => x.Stamina);

        staminaNew.Decrease(strong);
        staminaNew.Reset();

        Assert.AreEqual(shootsConfig.MaxStamina, staminaNew.Current);
    }
}
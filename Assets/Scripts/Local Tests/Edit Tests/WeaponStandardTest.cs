using Zenject;
using NUnit.Framework;
using System.Threading.Tasks;
using Moq;
using Assets.Resources.Data;
using Assets.Scripts.Shared;
using System.Linq;

[TestFixture]
public class WeaponStandardTest : ZenjectUnitTestFixture
{
    [SetUp]
    public override void Setup()
    {
        base.Setup();

        SignalBusInstaller.Install(Container);

        Container.DeclareSignal<ShootChangeSignal>();

        Container.Bind<MonkeySharedConfig>().FromScriptableObjectResource("Data/MonkeySharedConfig").AsSingle();
        Container.Bind<MonkeyConfig>().FromScriptableObjectResource("Data/Monkey Right").AsSingle();

        Container.Bind<IMove>().FromMock();

        var stamina = new Mock<IStamina>();
        stamina.Setup(x => x.HasStamina(It.IsAny<float>())).Returns(true);
        Container.BindInstance(stamina.Object);

        Container.BindInterfacesAndSelfTo<WeaponStandard>().AsSingle();
    }

    [Test]
    public async Task Shoot_Fast()
    {
        var weapon = Container.Resolve<WeaponStandard>();
        var shootsConfig = Container.Resolve<MonkeySharedConfig>();
        var shootFast = shootsConfig.ShootsStardard.Min(x => x);

        weapon.FireStart();
        await Task.Delay(shootFast.To - 100);
        weapon.FireStop();

        _ = weapon.TryGetShoot(out var shootActual);

        Assert.AreEqual(shootActual, shootFast);
    }


    [Test]
    public async Task Shoot_Strong()
    {
        var weapon = Container.Resolve<WeaponStandard>();
        var shootsConfig = Container.Resolve<MonkeySharedConfig>();
        var shootStrong = shootsConfig.ShootsStardard.Max(x => x);

        weapon.FireStart();
        await Task.Delay(shootStrong.From);
        weapon.FireStop();

        _ = weapon.TryGetShoot(out var shootActual);

        Assert.AreEqual(shootActual, shootStrong);
    }
}
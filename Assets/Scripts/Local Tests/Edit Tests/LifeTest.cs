using Zenject;
using NUnit.Framework;
using Assets.Scripts.Local;
using UnityEngine;
using Assets.Resources.Data;
using Assets.Scripts.Shared;

[TestFixture]
public class LifeTest : ZenjectUnitTestFixture
{
    [SetUp]
    public override void Setup()
    {
        base.Setup();

        SignalBusInstaller.Install(Container);
        Container.DeclareSignal<LifeDecreaseSignal>().OptionalSubscriber();
        Container.DeclareSignal<LifeResetSignal>().OptionalSubscriber();

        var gameObject = new GameObject();
        Container.Bind<MonkeySharedConfig>().FromScriptableObjectResource("Data/MonkeySharedConfig").AsSingle();
        Container.InstantiateComponent<Animator>(gameObject);
        Container.Bind<Animator>().FromComponentOn(gameObject).AsSingle();
        Container.InstantiateComponent<AudioSource>(gameObject);
        Container.Bind<AudioSource>().FromComponentOn(gameObject).AsSingle();

        Container.Bind<Life>().AsSingle();
    }

    [TearDown]
    public override void Teardown()
    {
        base.Teardown();
    }


    [Test]
    public void Life_Decrease()
    {
        var life = Container.Resolve<Life>();

        life.Decrease();

        Assert.AreEqual(4, life.value);
    }

    [Test]
    public void Life_Reset()
    {
        var life = Container.Resolve<Life>();
        var monkeyConfig = Container.Resolve<MonkeySharedConfig>();

        life.Decrease();
        life.Reset();

        Assert.AreEqual(monkeyConfig.life, life.value);
    }
}
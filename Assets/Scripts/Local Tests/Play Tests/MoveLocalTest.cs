using System.Collections;
using Assets.Resources.Data;
using Assets.Scripts.Local;
using Assets.Scripts.Shared;
using Moq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.TestTools;

[Ignore("USE MOCK")]
public class MoveLocalTest : InputTestFixture
{
    private GameObject _monkey;
    private InputAction _moveAction;
    private MoveLocal _monkeyMoveLocal;
    private Rigidbody2D _monkeyRigidbody;
    private Animator _monkeyAnimator;

    public override void Setup()
    {
        base.Setup();

        var monkeySharedConfig = Resources.Load<MonkeySharedConfig>("Data/MonkeySharedConfig"); 
        _monkey = new GameObject();
        _monkeyRigidbody = _monkey.AddComponent<Rigidbody2D>();
        _monkeyAnimator = _monkey.AddComponent<Animator>();
        _monkeyAnimator.runtimeAnimatorController =
            Resources.Load<RuntimeAnimatorController>("Animation/Game/MonkeyLeft");
        _monkeyMoveLocal = _monkey.AddComponent<MoveLocal>();
        var monkeyNew = new Move(_monkeyRigidbody, monkeySharedConfig, _monkeyAnimator, _monkey.AddComponent<RectTransform>());
        var weaponMock = new Mock<IWeaponsHandler>();
        _monkeyMoveLocal.Construct(monkeyNew, weaponMock.Object);
        _moveAction = new InputAction("MoveLocal");

        _moveAction.AddCompositeBinding("Dpad")
            .With("Up", "<Keyboard>/w")
            .With("Down", "<Keyboard>/s");
        _moveAction.Enable();

        _moveAction.started += OnMoveActionOnstarted;
        _moveAction.performed += OnMoveActionOnstarted;
        _moveAction.canceled += OnMoveActionOnstarted;
    }

    public override void TearDown()
    {
        _moveAction.started -= OnMoveActionOnstarted;
        _moveAction.performed -= OnMoveActionOnstarted;
        _moveAction.canceled -= OnMoveActionOnstarted;
        base.TearDown();
    }

    void OnMoveActionOnstarted(InputAction.CallbackContext context) => _monkeyMoveLocal.OnMovement(context);


    [Test]
    public void Move_Freeze()
    {
        _monkeyMoveLocal.SetFreeze(true);

        Assert.AreEqual(true, _monkeyRigidbody.constraints == RigidbodyConstraints2D.FreezeAll);
    }

    [UnityTest]
    public IEnumerator Move_KeyBoard_Up()
    {
        var keyboard = InputSystem.AddDevice<Keyboard>();

        Press(keyboard.wKey);

        yield return new WaitUntil(() => _moveAction.IsInProgress());
        yield return new WaitForFixedUpdate();

        Assert.That(_monkeyRigidbody.linearVelocity.y, Is.GreaterThan(0));
    }

    [UnityTest]
    public IEnumerator Move_KeyBoard_Down()
    {
        var keyboard = InputSystem.AddDevice<Keyboard>();

        Press(keyboard.sKey);

        yield return new WaitUntil(() => _moveAction.IsInProgress());
        yield return new WaitForFixedUpdate();

        Assert.That(_monkeyRigidbody.linearVelocity.y, Is.LessThan(0));
    }

    [UnityTest]
    public IEnumerator Move_Animation_Start()
    {
        var keyboard = InputSystem.AddDevice<Keyboard>();

        Press(keyboard.sKey);

        yield return new WaitUntil(() => _moveAction.IsInProgress());

        Assert.That(_monkeyAnimator.GetBool("IsMove"), Is.EqualTo(true));
    }


    [UnityTest]
    public IEnumerator Move_Animation_Stop()
    {
        var keyboard = InputSystem.AddDevice<Keyboard>();

        PressAndRelease(keyboard.sKey);

        yield return new WaitUntil(() => !_moveAction.IsInProgress());

        Assert.That(_monkeyAnimator.GetBool("IsMove"), Is.EqualTo(false));
    }
}

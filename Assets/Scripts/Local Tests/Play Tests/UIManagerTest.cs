using System.Collections;
using Assets.Resources.Data;
using Assets.Scripts.Local.UI;
using Assets.Scripts.Shared.UI;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using Scene = UnityEngine.SceneManagement.Scene;

public class UIManagerTest
{
    private bool _loaded;
    private UIManager _gameManager;

    [OneTimeSetUp]
    public void Setup()
    {
        SceneManager.LoadScene("BananaLocalScene");
        SceneManager.sceneLoaded += OnsceneLoaded;
    }

    [OneTimeTearDown]
    public void TeardDown()
    {
        SceneManager.sceneLoaded -= OnsceneLoaded;
    }

    private void OnsceneLoaded(Scene x, LoadSceneMode y)
    {
        _gameManager = GameObject.Find("GameManager").GetComponent<UIManager>();
        _loaded = true;
    }
    
    [UnityTest]
    public IEnumerator Game_Show()
    {
        yield return new WaitUntil(() => _loaded);
        
        var game = Object.FindFirstObjectByType<Game>(FindObjectsInactive.Exclude);

        _gameManager.ShowGame();

        Assert.AreEqual(true, game.IsVisible);
    }

    [UnityTest] public IEnumerator UI_Pause_Show()
    {
        yield return new WaitUntil(() => _loaded);

        var uiPause = Object.FindFirstObjectByType<UIPause>(FindObjectsInactive.Include);

        _gameManager.ShowPause();

        Assert.AreEqual(true, uiPause.IsVisible);
    }

    [UnityTest]
    public IEnumerator UI_Credits_Show()
    {
        yield return new WaitUntil(() => _loaded);

        var uiCredits = Object.FindFirstObjectByType<UICredits>(FindObjectsInactive.Include);

        _gameManager.ShowCredits();

        Assert.AreEqual(true, uiCredits.IsVisible);
    }

    [UnityTest]
    public IEnumerator UI_GameOver_Show()
    {
        yield return new WaitUntil(() => _loaded);

        var uiGameOver = Object.FindFirstObjectByType<UIGameEnd>(FindObjectsInactive.Include);

        _gameManager.ShowGameEnd(PlayerEnum.player1);

        Assert.AreEqual(true, uiGameOver.IsVisible);
    }

    //[Test]
    //public void ShowGame()
    //{
    //    var game = GameObject.Find("Game").GetComponent<Game>();
    //    _sut.ShowGame();
    //    Assert.AreEqual(true, game.IsVisible);
    //}
}

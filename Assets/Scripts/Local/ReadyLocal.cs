using Assets.Scripts.Shared;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace Assets.Scripts.Local
{
    public class ReadyLocal : MonoBehaviour, IReady
    {
        private Ready _ready;
        private SignalBus _signalBus;

        [Inject]
        public void Constructor(Ready ready, SignalBus signalBus)
        {
            _signalBus = signalBus;
            _ready = ready;
        }

        public void OnReady(InputAction.CallbackContext obj)
        {
            if (_ready.IsReady) return;

            if (obj.started) ReadyStart();
            if (obj.canceled) ReadyReset();
        }

        void Update()
        {
            if (_ready.value > 0)
                _signalBus.Fire(new ReadyChangeSignal { value = _ready.value });
        }

        public void ReadyReset()
        {
            _ready.Reset();
        }

        public void ReadyStart()
        {
            _ready.Start();
        }
    }
}

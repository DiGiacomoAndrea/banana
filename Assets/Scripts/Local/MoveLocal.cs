using Assets.Scripts.Shared;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace Assets.Scripts.Local
{
    public class MoveLocal : MonoBehaviour
    {
        private Vector2 _moveDirection;
        private Move _move;
        private IWeaponsHandler _weaponHandler;

        [Inject]
        public void Construct(Move move, IWeaponsHandler weaponsHandler)
        {
            _move = move;
            _weaponHandler = weaponsHandler;
        }

        void Awake()
        {
            _weaponHandler.OnShootStart += WeaponHandler_OnShootStart;
            _weaponHandler.OnShootEnd += WeaponHandler_OnShootEnd;
        }

        void OnDestroy()
        {
            _weaponHandler.OnShootStart += WeaponHandler_OnShootStart;
            _weaponHandler.OnShootEnd += WeaponHandler_OnShootEnd;
        }

        private void WeaponHandler_OnShootStart() => _move.SetFreeze(true);

        private void WeaponHandler_OnShootEnd() => _move.SetFreeze(false);

        public void OnMovement(InputAction.CallbackContext context)
        {
            if (!enabled) return;

            _moveDirection = context.ReadValue<Vector2>();
            _move.ExecuteAnimation(context.performed);
        }

        public void SetFreeze(bool value)
        {
            _move.SetFreeze(value);
        }

        public void FixedUpdate()
        {
            _move.Execute(_moveDirection);
        }

        void Pause(bool value)
        {
            enabled = !value;
            if (!value) return;

            _moveDirection = Vector2.zero;
            _move.SetMoveAnim(false);
        }

        void Reset()
        {
            _moveDirection = Vector2.zero;
            _move.Reset();
        }
    }
}
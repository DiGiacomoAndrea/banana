using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Shared;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputDeviceManager : MonoBehaviour
{
    [SerializeField] private List<PlayerInput> _playerInputs;

    void OnEnable()
    {
        var keyboardAndMouse = InputSystem.devices.Where(d => d is Mouse || d is Keyboard).ToArray();

        //TOUND force create two user with different control schema then reset to keyboardboard
        foreach (var playerInput in _playerInputs) playerInput.SwitchCurrentControlScheme(keyboardAndMouse);

        InputHelper.PairPlayerInputGamepads(_playerInputs.ToArray());
        InputSystem.onDeviceChange += OnDeviceChangeHandler;
    }

    void OnDestroy()
    {
        InputSystem.onDeviceChange -= OnDeviceChangeHandler;
    }

    void OnDeviceChangeHandler(InputDevice device, InputDeviceChange change)
    {
        InputHelper.DeviceChangeHandler(_playerInputs, device, change);
    }
}

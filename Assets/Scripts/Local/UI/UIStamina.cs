using Assets.Scripts.Local;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UIStamina : MonoBehaviour
{
    [SerializeField] private Slider _slider;

    public void UpdateSlider(float stamina) => _slider.value = stamina;
}

﻿using System.Collections.Generic;
using Assets.Resources.Data;
using Assets.Scripts.Shared.UI;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Local.UI
{
    public class UIManager : MonoBehaviour, IUIManager
    {

        [SerializeField] private GameObject _uiGameOver;
        [SerializeField] private GameObject _uiGameEnd;
        [SerializeField] private GameObject _uiGame;
        [SerializeField] private GameObject _uiPause;
        [SerializeField] private GameObject _uiCredits;
        [SerializeField] private GameObject _game;
        private List<IScene> _uiGameScene;

        public void Awake()
        {
            Physics2D.simulationMode = SimulationMode2D.FixedUpdate; //TODO

            _uiGameScene = new List<IScene>
            {
                _game.GetComponent<IScene>(),
                _uiGame.GetComponent<IScene>(),
                _uiPause.GetComponent<IScene>(),
                _uiCredits.GetComponent<IScene>(),
                _uiGameEnd.GetComponent<IScene>(),
                _uiGameOver.GetComponent<IScene>()
            };

            ShowGame();
        }

        public void ShowPause(InputAction.CallbackContext context)
        {
            if (!context.performed) return;

            if (_uiPause.GetComponent<IScene>().IsVisible)
                ShowGame();
            else
                ShowPause();
        }

        public void ShowPause()
        {
            _uiGameScene.SetHides((list) =>
            {
                list.GetType<UIPause>().Show();
                list.GetType<Game>().Show();
                list.GetType<Game>().SetPause(true);
            });
        }

        public void ShowGame()
        {
            _uiGameScene.SetHides((list) =>
            {
                list.GetType<UIGame>().Show();
                list.GetType<Game>().SetPause(true);
                list.GetType<UIGame>().StartCountdown();
                list.GetType<Game>().Show();
            });
        }

        public void ShowCredits()
        {
            _uiGameScene.SetHides((list) =>
            {
                list.GetType<UICredits>().Show();
                list.GetType<Game>().ActiveEasterEgg();
            });
        }

        public void ResetSceneGameOver()
        {
            _uiGameScene.GetType<Game>()!.ResetSceneGameOver();
            _uiGameScene.GetType<UIGameEnd>()!.ResetSceneGameOver();
            ResetScene();
        }

        public void ResetScene()
        {
            _uiGameScene.GetType<Game>()!.ResetScene();
            ShowGame();
        }


        public void ShowGameEnd(PlayerEnum loserPlayer)
        {
            _uiGameScene.SetHides((list) =>
            {
                list.GetType<Game>().SetGameEnd();
                list.GetType<UIGameEnd>().Show(loserPlayer);
                list.GetType<Game>().Show();
            });
        }

        public void ShowGameOver(PlayerEnum loserPlayer)
        {
            _uiGameScene.SetHides((list) =>
            {
                list.GetType<Game>().SetGameEnd();
                list.GetType<Game>().Show();
                list.GetType<UIGameOver>().Show(loserPlayer);
            });
        }

        public void ExitGame() => SceneManager.LoadScene("Home");

    }

}

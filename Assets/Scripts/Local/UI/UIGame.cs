﻿using Assets.Scripts.Shared.UI;
using Assets.Scripts.Shared.Utils;
using UnityEngine;

namespace Assets.Scripts.Local.UI
{
    public class UIGame : Scene
    {
        [SerializeField] private Timer _countdown;
        
        public void StartCountdown()
        {
            _countdown.StartCountdown();
        }
    }
}
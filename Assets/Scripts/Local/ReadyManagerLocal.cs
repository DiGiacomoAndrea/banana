using Assets.Scripts.Shared.UI;
using UnityEngine;
using Zenject;

public class ReadyManagerLocal : MonoBehaviour
{
    private IUIManager _uiManager;
    private readonly int _playerNumber = 2;
    private int _readySignalCount;

    [Inject]
    public void Constructor(IUIManager uiManager)
    {
        _uiManager = uiManager;
    }

    public void ReadyChangeSignalHandler()
    {
        _readySignalCount++;
        if (_readySignalCount == _playerNumber)
        {
            _uiManager.ResetScene();
            _readySignalCount = 0;
        }
    }
}

using Assets.Scripts.Shared;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

namespace Assets.Scripts.Local
{
    public class WeaponLocal : MonoBehaviour
    {
        [SerializeField] private Transform _weaponPoint;
        [SerializeField] private BulletStandardLocal _bulletStandard;
        [SerializeField] private BulletIceLocal _bulletIce;
        private IWeapon _selectedWeapon;
        private IWeaponsHandler _weaponsHandler;

        [Inject]
        public void Constructor(IWeaponsHandler weaponsHandler)
        {
            _weaponsHandler = weaponsHandler;
        }

        void Awake()
        {
            _selectedWeapon = _weaponsHandler.GetCurrentWeapon();
        }

        public void OnFire(InputAction.CallbackContext context)
        {
            if (!enabled) return;

            if (context.started) _selectedWeapon.FireStart();
            if (context.canceled)
            {
                _selectedWeapon.FireStop(); 

                if (_selectedWeapon.TryGetShoot(out var shoot))
                {
                    _selectedWeapon.DecreaseStamina(shoot);
                    var bulletVelocity = _selectedWeapon.GetVelocity(shoot);
                    var bullet = Instantiate(_selectedWeapon.Bullet, _weaponPoint.position, _weaponPoint.rotation, gameObject.transform.parent);
                    bullet.Launch(bulletVelocity);
                }

                _selectedWeapon.FireReset();
            }
        }

        public void OnChange(InputAction.CallbackContext context)
        {
            if (!context.performed) return;

            var weapon = _weaponsHandler.ChangeWeapon();

            _selectedWeapon = weapon;
        }

        void Pause(bool value)
        {
            enabled = !value;
            _weaponsHandler.Pause(value);
        }

        void Reset() => _weaponsHandler.Reset();
    }

}

using Assets.Resources.Data;
using Assets.Scripts.Shared;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Assets.Scripts.Local
{
    public class LifeLocal : MonoBehaviour
    {
        [SerializeField] private UnityEvent<PlayerEnum> _onGameEnd;
        [SerializeField] private UnityEvent<PlayerEnum> _onGameOver;
        private MonkeyConfig _monkeyConfig;
        private Life _life;
        private GameStateManager _gameStateManager;
        private Win _win;

        [Inject]
        public void Construnctor(Life life, Win win, MonkeyConfig monkeyConfig, GameStateManager gameStateManager)
        {
            _monkeyConfig = monkeyConfig;
            _win = win;
            _life = life;
            _gameStateManager = gameStateManager;
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            if (_gameStateManager.IsGameOver) return; 

            if (collision.gameObject.name.Contains("Banana"))
            {
                _life.Decrease();

                if (_life.value == 0)
                {
                    _gameStateManager.SetGameOver(true);

                    _win.Decrease();

                    if (_win.LooseCount == 0)
                        _onGameOver.Invoke(_monkeyConfig.player);
                    else
                        _onGameEnd.Invoke(_monkeyConfig.player);

                }
            }
        }

        private void Reset()
        {
            _gameStateManager.Reset();
            _life.Reset();
        }

        private void ResetGameOver()
        {
            _gameStateManager.Reset();
            _win.Reset();
        }
    }
}

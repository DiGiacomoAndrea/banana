using System.Collections.Generic;
using Assets.Scripts.Local;
using Assets.Scripts.Shared;
using Assets.Scripts.Shared.UI;
using UnityEngine;

public class LocalSceneInstaller : SceneInstaller
{
    [SerializeField] private ReadyLocal monkeyLeftReady;
    [SerializeField] private ReadyLocal monkeyRightReady;

    public override void InstallBindings()
    {
        base.InstallBindings();

        Container.Bind<IUIManager>().FromComponentInHierarchy().AsSingle();
        Container.Bind<ReadyManager>().ToSelf().AsSingle();
        Container.Bind<IReady>().FromMethodMultiple(_ => new List<ReadyLocal> { monkeyLeftReady, monkeyRightReady }).AsSingle();
    }
}
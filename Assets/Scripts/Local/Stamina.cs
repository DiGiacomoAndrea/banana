using System.Collections;
using Assets.Scripts.Shared;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Local
{
    public class Stamina : MonoBehaviour
    {
        private StaminaNew _staminaNew;
        private SignalBus _signalBus;

        [Inject]
        public void Constructor(StaminaNew staminaNew, SignalBus signalBus)
        {
            _staminaNew = staminaNew;
            _signalBus = signalBus;
        }

        void Start() => _signalBus.Subscribe<StaminaRigenerateSignal>(RigenerateStaminaHandler);
        void Destroy() => _signalBus.Unsubscribe<StaminaRigenerateSignal>(RigenerateStaminaHandler);

        void OnEnable()
        {
            if (_staminaNew.Current < _staminaNew.Max)
                StartCoroutine("RigenerateStamina");
        }

        public void RigenerateStaminaHandler()
        {
            StopCoroutine("RigenerateStamina");
            StartCoroutine("RigenerateStamina");
        }

        private IEnumerator RigenerateStamina()
        {
            yield return new WaitForSeconds(0.7f);

            while (_staminaNew.Current < _staminaNew.Max)
            {
                _staminaNew.Add(2);
                yield return new WaitForFixedUpdate();
            }
        }

        private void Reset() => _staminaNew.Reset();
    }
}

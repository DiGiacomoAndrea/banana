using Assets.Scripts.Shared;
using Assets.Scripts.Shared.Bullets;
using UnityEngine;

namespace Assets.Scripts.Local
{
    public class BulletIceLocal : BulletIce
    {
        [SerializeField] private Rigidbody2D _rigidbody2D;
        [SerializeField] private ParticleSystem _particleIce;

        void Awake()
        {
            BulletBase = new BulletICeLogic(_rigidbody2D, _particleIce);
        }

        void OnCollisionEnter2D(Collision2D collision2D)
        {
            if (GetIsToDestroy(collision2D))
            {
                Destroy(gameObject);
            }

            if (GetIsToSlow(collision2D))
            {
                SetSlow();
            }
        }

    }
}
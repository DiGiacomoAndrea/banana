using Assets.Scripts.Shared;
using Assets.Scripts.Shared.Bullets;
using UnityEngine;

namespace Assets.Scripts.Local
{
    public class BulletStandardLocal : Bullet
    {
        [SerializeField] private Rigidbody2D _rigidbody2D;
        [SerializeField] private ParticleSystem _particleFast;
        [SerializeField] private ParticleSystem _particleIce;

        void Awake()
        {
            BulletBase = new BulletStandardLogic(_rigidbody2D, _particleFast, _particleIce);
        }

        //OnCollisionStay2D not work properly? why?
        void OnCollisionEnter2D(Collision2D collision2D)
        {
            if (GetIsToDestroy(collision2D))
            {
                Destroy(gameObject);
            }

            if (GetIsToSlow(collision2D))
            {
                SetSlow();
                ShowParticleIce();
            }
        }
    }
}
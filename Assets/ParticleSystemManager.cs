using UnityEngine;

public class ParticleSystemManager : MonoBehaviour
{
    #region Type

    private enum SyncType
    {
        Position,
        Velocity
    }

    private struct ParticlePositionRange
    {
        //direction x1 y0
        //position -x1 y0

        //direction x0 y1
        //position x0 y0.2

        //direction x0 y0.5
        //position x15 y4
        public Vector2 MaxPosition => new(0.5f, 0.2f);
    }

    #endregion

    [SerializeField] private SyncType _syncType;
    [SerializeField] private Rigidbody2D _rigidbody2D;

    private ParticleSystem _particleSystem;
    private ParticlePositionRange _positionRange;

    void Start()
    {
        _particleSystem = GetComponent<ParticleSystem>();
        _positionRange = new ParticlePositionRange();
    }

    void Update()
    {
        var rigidbody2DVelocity = _rigidbody2D.linearVelocity;

        if (_syncType == SyncType.Velocity)
        {
            SyncPositionByVelocity(rigidbody2DVelocity);
        }
        else
        {
            SyncPositionByPosition(_rigidbody2D.position);
        }
    }

    private void SyncPositionByPosition(Vector2 position)
    {
        _particleSystem.transform.position = position;
    }

    private void SyncPositionByVelocity(Vector2 rigidbody2DVelocity)
    {
        var direction = rigidbody2DVelocity.normalized;

        // Imposta la direzione delle particelle opposta alla velocità del Rigidbody
        _particleSystem.transform.rotation = Quaternion.LookRotation(-rigidbody2DVelocity);

        // Imposta la posizione della particella con un offset che dipende dalla direzione
        _particleSystem.transform.position = new Vector2(
            _rigidbody2D.position.x - (_positionRange.MaxPosition.x * direction.x), 
            _rigidbody2D.position.y - (_positionRange.MaxPosition.y * direction.y));
    }
}

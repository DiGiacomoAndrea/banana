mergeInto(LibraryManager.library, {
  TextCopyWeb: function (text) {
    let decodedString = UTF8ToString(text); 
    if (window.isSecureContext && navigator.clipboard) {
        console.log("Web secure copyToClipboard");
        navigator.clipboard.writeText(decodedString);
    } else {
        console.log("Web unsecure copyToClipboard");
        const textArea = document.createElement("textarea");
        textArea.value = decodedString;
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
        try {
            document.execCommand('copy');
        } catch (err) {
            console.error('Unable to copy to clipboard', err);
        }
        document.body.removeChild(textArea);
    }
  },
});


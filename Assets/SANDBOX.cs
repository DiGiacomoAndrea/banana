using Assets.Resources.Data;
using UnityEngine;
using UnityEngine.Events;

public class SANDBOX : MonoBehaviour
{
    [SerializeField] private UnityEvent<PlayerEnum> action;

    public void Execute()
    {
        action?.Invoke(PlayerEnum.player1);
    }
}
